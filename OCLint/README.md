#OCLint

###OCLint介绍

OCLint是一个静态代码分析器，可以检测的Objective-C代码(同时支持Ç和c++),发现常见的问题:例如的if/else/try/catch,最后声明出错,未被使用的本地实例和参数,过度复杂（有效值代码行数状语从句:常循环复杂度太高）,冗余代码,代码异味,以及其他不好的代码。

代码异味:程序开发领域，代码中的任何可能导致深层次问题的症状都可以叫做代码异味
常见如下:

代码重复: 相同或者相似的代码存在于一个以上的地方。

长方法: 一个非常长的方法、函数或者过程。

巨类: 一个非常庞大的类。

太多的参数: 函数或者过程的冗长的参数列表使得代码可读性和质量非常差。

特性依恋: 一个类过度的使用另一个类的方法。

亲密关系: 一个类依赖另一个类的实现细节。

拒绝继承: 子类以一种‘拒绝’的态度，覆盖基类中的方法，换句话说，子类不想继承父类中的方法。

冗余类 / 寄生虫: 一个功能太少的类。

人为的复杂: 在简单设计已经满足需求的时候，强迫使用极度复杂的设计模式。

超长标识符: 尤其，在软件工程中，应该毫无保留的使用命名规则来消除歧义。

超短标识符: 除非很明显，一个变量名应该反映它的功用。

过度使用字面值: 为提高可读性和避免编码错误，应该使用命名常量。此外，字面值可以且应该在可能的情况下，独立存放于资源文件或者脚本中，在软件部署到不同区域时，可以很方便的本地化。

###OCLint+xcodebulid+xcpretty完成代码检测

####环境准备

#####1.安装OCLint
解压文件夹中的oclint-0.10.2.zip

(如果需要自行下载，也可到https://github.com/oclint/oclint/releases   下载 darwin 版,下载完成后解压)

找到 /User/xxx/.bash_profile,打开这个脚本文件,将添加OCLint解压出来的路径添加到环境变量。

即在该脚本最后添加以下代码,其中/path/to/oclint-release 是执行文件存放路径，
例如：/Users/xxx/Desktop/oclint-0.10.2

        OCLINT_HOME=/path/to/oclint-release  
        export PATH=$OCLINT_HOME/bin:$PATH

然后记得保存一下,打开终端，调用一下

        oclint

如果显示如下，那么oclint 即安装成功
        oclint: Not enough positional command line arguments specified!
        Must specify at least 1 positional arguments: See: oclint -help


(上述如果没有找到.bash_profile,请自行创建:
i.进入当前用户根目录:
        cd ~/

ii.创建.bash_profile
        touch .bash_profile

iii.编辑.bash_profile文件
        open .bash_profile
然后将上面提到的两行代码添加到文件中

iv.保存，关闭文件

v.更新刚配置的环境变量
        source .bash_profile
)

#####2.安装xcpretty

打开终端，执行

        gem install xcpretty

如果没有权限那么就添加sudo:

        sudo gem install xcpretty

完事后可以查看一下xvpretty版本检测是否安装成功

        xcpretty —version

####应用到项目中

1.为需要进行OCLint检测的项目新增Target,类型为Aggregate,可以将Target命名为OCLint

2.新的Target - Build Phases 中添加RunScript,并添加以下脚本

        source ~/.bash_profile
        cd ${SRCROOT}
        #clean 项目
        xcodebuild clean
        #使用后xcpretty,获取json
        xcodebuild | xcpretty -r json-compilation-database
        #复制json文件
        cp Build/reports/compilation_db.json compile_commands.json
        #通过json文件,生成HTML报告
        oclint-json-compilation-database -- -report-type html -o oclint_result.html
        #删除json文件
        rm compile_commands.json

3.运行Target,显示的红色警告即为代码问题地方,也可以在Xcode左侧工具栏的最后一个查看分析结果。同时在项目根目录会生成 oclint_result.html的文件，就是对项目的静态分析报告

######注:如果项目的名字带了中文，那么将会无法生成html文件.