//
//  ClassA.m
//  OCLintDemo
//
//  Created by chenjd on 16/4/13.
//  Copyright © 2016年 Mob. All rights reserved.
//

#import "ClassA.h"

@implementation ClassA


- (void)aMethodOfClassA
{
    NSLog(@"Class A - aMethod");
}

@end
