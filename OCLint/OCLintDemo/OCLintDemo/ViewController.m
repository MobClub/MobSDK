//
//  ViewController.m
//  OCLintDemo
//
//  Created by chenjd on 16/4/12.
//  Copyright © 2016年 Mob. All rights reserved.
//

#import "ViewController.h"
#import "ClassA.h"
#import "ClassB.h"
@interface ViewController ()

@property (nonatomic, copy) NSString *myStr;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *aString = @"unused";
    
    [self alwaysYES];

    [self emptyIfElse];
    
    [self infiniteLoop];
    
    [self neverBeExecuted];
    
    
    ClassA *classA = [[ClassA alloc]init];
    [classA aMethodOfClassA];
    
    [ClassB aMethodOfClassB];
    
    
    [ClassB aMethodWithManyParams:@"12"
                          paramsA:@"12"
                          paramsB:@"12"
                          paramsC:@"12"
                          paramsD:@"12"
                          paramsE:@"12"
                          paramsF:@"12"
                          paramsG:@"12"
                          paramsH:@"12"
                          paramsI:@"12"
                          paramsI:@"12"
                          paramsI:@"12"
                          paramsI:@"12"
                          paramsI:@"12"
                          paramsI:@"12"
                          paramsI:@"12"
                          paramsI:@"12"
                          paramsI:@"12"];
    
    
}

- (void)alwaysYES
{
    if (YES)
    {
        NSLog(@"always YES");
        
    }

}

- (void)emptyIfElse
{
    if (NO)
    {
        
    }
    else
    {
        
    }
    
    @try
    {
        
    }
    @catch(NSException *error)
    {
        
    }
}

- (void)infiniteLoop
{
    while (1)
    {
        NSLog(@"111");
    }
}

- (void)neverBeExecuted
{
    return;
    NSLog(@"Your will never be here!");
    
}

@end
