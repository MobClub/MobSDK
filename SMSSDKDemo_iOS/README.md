#SMSSDKDemo-iOS

项目实现：李健 

一、项目依赖

本工程采用CocoaPods 管理

通过 CocoaPods进行安装，只需在 Podfile文件中添加，添加之后执行 pod install 命令

// Mob产品公共库
pod 'MOBFoundation_IDFA'
// SMSSDK必须
pod 'SMSSDK'


二、注意

如果项目中同时集成了Mob其它产品，只要集成SMSSDK中的IDFA版本MOBFoundation.framework即可

![Usage](./Untitled.gif)
