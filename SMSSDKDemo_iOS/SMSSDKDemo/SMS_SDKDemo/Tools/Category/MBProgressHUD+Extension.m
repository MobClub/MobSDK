//
//  MBProgressHUD+Extension.m
//  SMS_SDKDemo
//
//  Created by LeeJay on 16/4/15.
//  Copyright © 2016年 LeeJay. All rights reserved.
//

#import "MBProgressHUD+Extension.h"

@implementation MBProgressHUD (Extension)

+ (MBProgressHUD *)showHUDOnView:(UIView *)view {
    if (view == nil) view = [[UIApplication sharedApplication].windows lastObject];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.labelColor = [UIColor whiteColor];
    hud.color = [UIColor blackColor];
    hud.labelFont = [UIFont systemFontOfSize:15];
    hud.removeFromSuperViewOnHide = YES;
    return hud;
}

+ (void)hideHUDForView:(UIView *)view {
    [self hideHUDForView:view animated:YES];
}

@end
