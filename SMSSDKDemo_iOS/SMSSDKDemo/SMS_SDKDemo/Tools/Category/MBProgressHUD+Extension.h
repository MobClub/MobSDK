//
//  MBProgressHUD+Extension.h
//  SMS_SDKDemo
//
//  Created by LeeJay on 16/4/15.
//  Copyright © 2016年 LeeJay. All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>

@interface MBProgressHUD (Extension)

+ (MBProgressHUD *)showHUDOnView:(UIView *)view;

+ (void)hideHUDForView:(UIView *)view;

@end
