﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class WebCamera : MonoBehaviour
{
   
    public GameObject plane;
    private Material mMaterial = null;

    private WebCamTexture mFrontWebcamTexture = null;
    private WebCamTexture mRearWebcamTexture = null;
    private WebCamDevice[] mDevices ;
    [HideInInspector]
    public WebCamTexture mActiveCam = null;
    
  

    void Start()
    {
		if(plane == null) return;
        mMaterial = plane.GetComponent<MeshRenderer>().material;
        
        if (mMaterial == null) return;
        plane.SetActive(false);
        StartCoroutine(GetCamera());
    }

    void OnDestroy()
    {
        StopCamera();
    }

    IEnumerator GetCamera()
    {
        yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);
        if (Application.HasUserAuthorization(UserAuthorization.WebCam))
        {
            mDevices = WebCamTexture.devices;
            if (mDevices != null && mDevices.Length > 0)
            {
                string frontCamName = "";
                string rearCamName = "";
                for (int i = 0; i < mDevices.Length; i++)
                {
                    if (mDevices[i].isFrontFacing)
                        frontCamName = mDevices[i].name;
                    else
                        rearCamName = mDevices[i].name;
                    Debug.Log(WebCamTexture.devices[i].name);
                }
                mFrontWebcamTexture = new WebCamTexture(frontCamName);
                mRearWebcamTexture = new WebCamTexture(rearCamName);
                mFrontWebcamTexture.Stop();
                mRearWebcamTexture.Stop();
                if (!string.IsNullOrEmpty(frontCamName))
                {
                    mActiveCam = mFrontWebcamTexture;
                    mMaterial.SetTexture("_MainTex", mFrontWebcamTexture);
                }
                else
                {
                    mActiveCam = mRearWebcamTexture;
                    mMaterial.SetTexture("_MainTex", mRearWebcamTexture);
                }
                //mMaterial.mainTexture = mRearWebcamTexture;
                plane.SetActive(true);
                mActiveCam.Play();
            }
        }
    }

    public void StartCamera()
    {
        if (mMaterial == null || mActiveCam == null)
            return;
        mActiveCam.Play();
    }

    public void StopCamera()
    {
        if (mMaterial == null || mActiveCam == null)
            return;
        mActiveCam.Stop();
    }

    public bool HasFrontCamera()
    {
        if (Application.isEditor)
            return false;
        return mFrontWebcamTexture.deviceName != "";
    }

    public void ChangeCamera()
    {
        if (!HasFrontCamera())
            return;
        mActiveCam.Stop();
        if (mActiveCam == mFrontWebcamTexture)
        {
            mMaterial.SetTexture("_MainTex", mRearWebcamTexture);
            mActiveCam = mRearWebcamTexture;
        }
        else
        {
            mMaterial.SetTexture("_MainTex", mFrontWebcamTexture);
            mActiveCam = mFrontWebcamTexture;
        }
        mActiveCam.Play();
    }

    public void SavePhoto(string name)
    {
        Texture tex = mMaterial.mainTexture;
        Texture2D tx = new Texture2D(mActiveCam.width, mActiveCam.height);
        tx.SetPixels(mActiveCam.GetPixels());
        byte[] byte_photo = tx.EncodeToPNG();
        string photoName = name + ".png";

        FileStream fs = new System.IO.FileStream("/mnt/sdcard/DCIM/Camera/"+ photoName, System.IO.FileMode.Create);
        fs.Write(byte_photo, 0, byte_photo.Length);
        fs.Close();
    }

}
