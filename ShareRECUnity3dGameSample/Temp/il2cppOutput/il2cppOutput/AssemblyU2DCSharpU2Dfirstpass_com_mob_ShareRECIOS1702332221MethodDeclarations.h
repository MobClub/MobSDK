﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// com.mob.FinishedRecordEvent
struct FinishedRecordEvent_t4162477592;
// System.Collections.Hashtable
struct Hashtable_t3875263730;
// com.mob.CloseEvent
struct CloseEvent_t4246496483;
// com.mob.EditResultEvent
struct EditResultEvent_t127194420;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_FinishedReco4162477592.h"
#include "mscorlib_System_Collections_Hashtable3875263730.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_CloseEvent4246496483.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_EditResultEve127194420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_SocialPageTyp127466071.h"

// System.Void com.mob.ShareRECIOS::.cctor()
extern "C"  void ShareRECIOS__cctor_m656406199 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::__iosShareRECRegisterApp(System.String)
extern "C"  void ShareRECIOS___iosShareRECRegisterApp_m1552351604 (Il2CppObject * __this /* static, unused */, String_t* ___appKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::__iosShareRECStartRecording()
extern "C"  void ShareRECIOS___iosShareRECStartRecording_m1115202625 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::__iosShareRECStopRecording(System.String)
extern "C"  void ShareRECIOS___iosShareRECStopRecording_m2450047363 (Il2CppObject * __this /* static, unused */, String_t* ___observer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::__iosShareRECPlayLastRecording()
extern "C"  void ShareRECIOS___iosShareRECPlayLastRecording_m433893239 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::__iosShareRECSetBitRate(System.Int32)
extern "C"  void ShareRECIOS___iosShareRECSetBitRate_m1854816430 (Il2CppObject * __this /* static, unused */, int32_t ___bitRate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::__iosShareRECSetFPS(System.Int32)
extern "C"  void ShareRECIOS___iosShareRECSetFPS_m3348572298 (Il2CppObject * __this /* static, unused */, int32_t ___fps, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String com.mob.ShareRECIOS::__iosShareRECLastRecordingPath()
extern "C"  String_t* ShareRECIOS___iosShareRECLastRecordingPath_m3348631437 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::__iosShareRECEditLastRecording(System.String,System.String,System.String)
extern "C"  void ShareRECIOS___iosShareRECEditLastRecording_m1045588665 (Il2CppObject * __this /* static, unused */, String_t* ___title, String_t* ___userData, String_t* ___observer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::__iosShareRECEditLastRecordingNew(System.String)
extern "C"  void ShareRECIOS___iosShareRECEditLastRecordingNew_m1854432001 (Il2CppObject * __this /* static, unused */, String_t* ___observer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::__iosShareRECSocialOpen(System.String,System.String,System.Int32,System.String)
extern "C"  void ShareRECIOS___iosShareRECSocialOpen_m2750311422 (Il2CppObject * __this /* static, unused */, String_t* ___title, String_t* ___userData, int32_t ___pageType, String_t* ___observer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::__iosShareRECSetMinimumRecordingTime(System.Single)
extern "C"  void ShareRECIOS___iosShareRECSetMinimumRecordingTime_m2095833641 (Il2CppObject * __this /* static, unused */, float ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::__iosShareRECSetSyncAudioComment(System.Boolean)
extern "C"  void ShareRECIOS___iosShareRECSetSyncAudioComment_m462655021 (Il2CppObject * __this /* static, unused */, bool ___syncAudioComment, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::setCallbackObjectName(System.String)
extern "C"  void ShareRECIOS_setCallbackObjectName_m3361143773 (Il2CppObject * __this /* static, unused */, String_t* ___objectName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::shareRECCallback(System.String)
extern "C"  void ShareRECIOS_shareRECCallback_m2673680446 (Il2CppObject * __this /* static, unused */, String_t* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::registerApp(System.String)
extern "C"  void ShareRECIOS_registerApp_m1021613200 (Il2CppObject * __this /* static, unused */, String_t* ___appKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::startRecording()
extern "C"  void ShareRECIOS_startRecording_m461515357 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::stopRecording(com.mob.FinishedRecordEvent)
extern "C"  void ShareRECIOS_stopRecording_m62795219 (Il2CppObject * __this /* static, unused */, FinishedRecordEvent_t4162477592 * ___evt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::playLastRecording()
extern "C"  void ShareRECIOS_playLastRecording_m4113179611 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::setBitRate(System.Int32)
extern "C"  void ShareRECIOS_setBitRate_m942533322 (Il2CppObject * __this /* static, unused */, int32_t ___bitRate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::setFPS(System.Int32)
extern "C"  void ShareRECIOS_setFPS_m1442332838 (Il2CppObject * __this /* static, unused */, int32_t ___fps, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::setMinimumRecordingTime(System.Single)
extern "C"  void ShareRECIOS_setMinimumRecordingTime_m215591749 (Il2CppObject * __this /* static, unused */, float ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String com.mob.ShareRECIOS::lastRecordingPath()
extern "C"  String_t* ShareRECIOS_lastRecordingPath_m130556759 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::editLastRecording(System.String,System.Collections.Hashtable,com.mob.CloseEvent)
extern "C"  void ShareRECIOS_editLastRecording_m4004253032 (Il2CppObject * __this /* static, unused */, String_t* ___title, Hashtable_t3875263730 * ___userData, CloseEvent_t4246496483 * ___evt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::editLastRecording(com.mob.EditResultEvent)
extern "C"  void ShareRECIOS_editLastRecording_m2968837237 (Il2CppObject * __this /* static, unused */, EditResultEvent_t127194420 * ___evt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::setSyncAudioComment(System.Boolean)
extern "C"  void ShareRECIOS_setSyncAudioComment_m936622737 (Il2CppObject * __this /* static, unused */, bool ___flag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareRECIOS::openSocial(System.String,System.Collections.Hashtable,com.mob.SocialPageType,com.mob.CloseEvent)
extern "C"  void ShareRECIOS_openSocial_m4253959253 (Il2CppObject * __this /* static, unused */, String_t* ___title, Hashtable_t3875263730 * ___userData, int32_t ___pageType, CloseEvent_t4246496483 * ___evt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
