﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// WebCamera
struct WebCamera_t387566457;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebCamera/<GetCamera>c__Iterator0
struct  U3CGetCameraU3Ec__Iterator0_t2229711262  : public Il2CppObject
{
public:
	// System.String WebCamera/<GetCamera>c__Iterator0::<frontCamName>__0
	String_t* ___U3CfrontCamNameU3E__0_0;
	// System.String WebCamera/<GetCamera>c__Iterator0::<rearCamName>__1
	String_t* ___U3CrearCamNameU3E__1_1;
	// System.Int32 WebCamera/<GetCamera>c__Iterator0::<i>__2
	int32_t ___U3CiU3E__2_2;
	// System.Int32 WebCamera/<GetCamera>c__Iterator0::$PC
	int32_t ___U24PC_3;
	// System.Object WebCamera/<GetCamera>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// WebCamera WebCamera/<GetCamera>c__Iterator0::<>f__this
	WebCamera_t387566457 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_U3CfrontCamNameU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetCameraU3Ec__Iterator0_t2229711262, ___U3CfrontCamNameU3E__0_0)); }
	inline String_t* get_U3CfrontCamNameU3E__0_0() const { return ___U3CfrontCamNameU3E__0_0; }
	inline String_t** get_address_of_U3CfrontCamNameU3E__0_0() { return &___U3CfrontCamNameU3E__0_0; }
	inline void set_U3CfrontCamNameU3E__0_0(String_t* value)
	{
		___U3CfrontCamNameU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfrontCamNameU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CrearCamNameU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetCameraU3Ec__Iterator0_t2229711262, ___U3CrearCamNameU3E__1_1)); }
	inline String_t* get_U3CrearCamNameU3E__1_1() const { return ___U3CrearCamNameU3E__1_1; }
	inline String_t** get_address_of_U3CrearCamNameU3E__1_1() { return &___U3CrearCamNameU3E__1_1; }
	inline void set_U3CrearCamNameU3E__1_1(String_t* value)
	{
		___U3CrearCamNameU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrearCamNameU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CiU3E__2_2() { return static_cast<int32_t>(offsetof(U3CGetCameraU3Ec__Iterator0_t2229711262, ___U3CiU3E__2_2)); }
	inline int32_t get_U3CiU3E__2_2() const { return ___U3CiU3E__2_2; }
	inline int32_t* get_address_of_U3CiU3E__2_2() { return &___U3CiU3E__2_2; }
	inline void set_U3CiU3E__2_2(int32_t value)
	{
		___U3CiU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CGetCameraU3Ec__Iterator0_t2229711262, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CGetCameraU3Ec__Iterator0_t2229711262, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3CGetCameraU3Ec__Iterator0_t2229711262, ___U3CU3Ef__this_5)); }
	inline WebCamera_t387566457 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline WebCamera_t387566457 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(WebCamera_t387566457 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
