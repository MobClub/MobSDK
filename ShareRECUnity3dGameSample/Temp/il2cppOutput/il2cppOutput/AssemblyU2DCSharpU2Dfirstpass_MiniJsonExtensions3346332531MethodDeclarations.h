﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t3875263730;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// System.Collections.ArrayList
struct ArrayList_t2121638921;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable3875263730.h"
#include "mscorlib_System_String968488902.h"

// System.String MiniJsonExtensions::toJson(System.Collections.Hashtable)
extern "C"  String_t* MiniJsonExtensions_toJson_m1205538118 (Il2CppObject * __this /* static, unused */, Hashtable_t3875263730 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MiniJsonExtensions::toJson(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  String_t* MiniJsonExtensions_toJson_m2635768255 (Il2CppObject * __this /* static, unused */, Dictionary_2_t2606186806 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList MiniJsonExtensions::arrayListFromJson(System.String)
extern "C"  ArrayList_t2121638921 * MiniJsonExtensions_arrayListFromJson_m2234629349 (Il2CppObject * __this /* static, unused */, String_t* ___json, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable MiniJsonExtensions::hashtableFromJson(System.String)
extern "C"  Hashtable_t3875263730 * MiniJsonExtensions_hashtableFromJson_m1478403219 (Il2CppObject * __this /* static, unused */, String_t* ___json, const MethodInfo* method) IL2CPP_METHOD_ATTR;
