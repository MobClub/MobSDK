﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Demo
struct  Demo_t2126339  : public MonoBehaviour_t3012272455
{
public:
	// System.Boolean Demo::_hasRecord
	bool ____hasRecord_2;

public:
	inline static int32_t get_offset_of__hasRecord_2() { return static_cast<int32_t>(offsetof(Demo_t2126339, ____hasRecord_2)); }
	inline bool get__hasRecord_2() const { return ____hasRecord_2; }
	inline bool* get_address_of__hasRecord_2() { return &____hasRecord_2; }
	inline void set__hasRecord_2(bool value)
	{
		____hasRecord_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
