﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// com.mob.CloseEvent
struct CloseEvent_t4246496483;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void com.mob.CloseEvent::.ctor(System.Object,System.IntPtr)
extern "C"  void CloseEvent__ctor_m2047460688 (CloseEvent_t4246496483 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.CloseEvent::Invoke()
extern "C"  void CloseEvent_Invoke_m3525735850 (CloseEvent_t4246496483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_CloseEvent_t4246496483(Il2CppObject* delegate);
// System.IAsyncResult com.mob.CloseEvent::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CloseEvent_BeginInvoke_m953735449 (CloseEvent_t4246496483 * __this, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.CloseEvent::EndInvoke(System.IAsyncResult)
extern "C"  void CloseEvent_EndInvoke_m1110728544 (CloseEvent_t4246496483 * __this, Il2CppObject * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
