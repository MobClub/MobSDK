﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebCamera/<GetCamera>c__Iterator0
struct U3CGetCameraU3Ec__Iterator0_t2229711262;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void WebCamera/<GetCamera>c__Iterator0::.ctor()
extern "C"  void U3CGetCameraU3Ec__Iterator0__ctor_m3968592227 (U3CGetCameraU3Ec__Iterator0_t2229711262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WebCamera/<GetCamera>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetCameraU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3016651535 (U3CGetCameraU3Ec__Iterator0_t2229711262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WebCamera/<GetCamera>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetCameraU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2483975331 (U3CGetCameraU3Ec__Iterator0_t2229711262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebCamera/<GetCamera>c__Iterator0::MoveNext()
extern "C"  bool U3CGetCameraU3Ec__Iterator0_MoveNext_m138777265 (U3CGetCameraU3Ec__Iterator0_t2229711262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebCamera/<GetCamera>c__Iterator0::Dispose()
extern "C"  void U3CGetCameraU3Ec__Iterator0_Dispose_m4079421664 (U3CGetCameraU3Ec__Iterator0_t2229711262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebCamera/<GetCamera>c__Iterator0::Reset()
extern "C"  void U3CGetCameraU3Ec__Iterator0_Reset_m1615025168 (U3CGetCameraU3Ec__Iterator0_t2229711262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
