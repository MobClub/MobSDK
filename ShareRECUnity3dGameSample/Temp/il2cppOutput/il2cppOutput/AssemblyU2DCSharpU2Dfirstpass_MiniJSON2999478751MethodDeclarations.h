﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MiniJSON
struct MiniJSON_t2999478751;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t3875263730;
// System.Char[]
struct CharU5BU5D_t3416858730;
// System.Collections.ArrayList
struct ArrayList_t2121638921;
// System.Text.StringBuilder
struct StringBuilder_t3822575854;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Text_StringBuilder3822575854.h"
#include "mscorlib_System_Collections_Hashtable3875263730.h"
#include "mscorlib_System_Collections_ArrayList2121638921.h"

// System.Void MiniJSON::.ctor()
extern "C"  void MiniJSON__ctor_m3723319888 (MiniJSON_t2999478751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MiniJSON::.cctor()
extern "C"  void MiniJSON__cctor_m3271670621 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MiniJSON::jsonDecode(System.String)
extern "C"  Il2CppObject * MiniJSON_jsonDecode_m2239788899 (Il2CppObject * __this /* static, unused */, String_t* ___json, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MiniJSON::jsonEncode(System.Object)
extern "C"  String_t* MiniJSON_jsonEncode_m3503903903 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___json, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MiniJSON::lastDecodeSuccessful()
extern "C"  bool MiniJSON_lastDecodeSuccessful_m4224849614 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MiniJSON::getLastErrorIndex()
extern "C"  int32_t MiniJSON_getLastErrorIndex_m2071674926 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MiniJSON::getLastErrorSnippet()
extern "C"  String_t* MiniJSON_getLastErrorSnippet_m3025045548 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable MiniJSON::parseObject(System.Char[],System.Int32&)
extern "C"  Hashtable_t3875263730 * MiniJSON_parseObject_m2461982797 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t* ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList MiniJSON::parseArray(System.Char[],System.Int32&)
extern "C"  ArrayList_t2121638921 * MiniJSON_parseArray_m46098480 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t* ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MiniJSON::parseValue(System.Char[],System.Int32&,System.Boolean&)
extern "C"  Il2CppObject * MiniJSON_parseValue_m1751092568 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t* ___index, bool* ___success, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MiniJSON::parseString(System.Char[],System.Int32&)
extern "C"  String_t* MiniJSON_parseString_m561858817 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t* ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double MiniJSON::parseNumber(System.Char[],System.Int32&)
extern "C"  double MiniJSON_parseNumber_m2389640457 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t* ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MiniJSON::getLastIndexOfNumber(System.Char[],System.Int32)
extern "C"  int32_t MiniJSON_getLastIndexOfNumber_m804658316 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MiniJSON::eatWhitespace(System.Char[],System.Int32&)
extern "C"  void MiniJSON_eatWhitespace_m240573997 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t* ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MiniJSON::lookAhead(System.Char[],System.Int32)
extern "C"  int32_t MiniJSON_lookAhead_m2447657014 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MiniJSON::nextToken(System.Char[],System.Int32&)
extern "C"  int32_t MiniJSON_nextToken_m3797447794 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t* ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MiniJSON::serializeObjectOrArray(System.Object,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeObjectOrArray_m1421528817 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___objectOrArray, StringBuilder_t3822575854 * ___builder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MiniJSON::serializeObject(System.Collections.Hashtable,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeObject_m2476982477 (Il2CppObject * __this /* static, unused */, Hashtable_t3875263730 * ___anObject, StringBuilder_t3822575854 * ___builder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MiniJSON::serializeDictionary(System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeDictionary_m2089695709 (Il2CppObject * __this /* static, unused */, Dictionary_2_t2606186806 * ___dict, StringBuilder_t3822575854 * ___builder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MiniJSON::serializeArray(System.Collections.ArrayList,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeArray_m1996381038 (Il2CppObject * __this /* static, unused */, ArrayList_t2121638921 * ___anArray, StringBuilder_t3822575854 * ___builder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MiniJSON::serializeValue(System.Object,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeValue_m1454356919 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value, StringBuilder_t3822575854 * ___builder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MiniJSON::serializeString(System.String,System.Text.StringBuilder)
extern "C"  void MiniJSON_serializeString_m2203421607 (Il2CppObject * __this /* static, unused */, String_t* ___aString, StringBuilder_t3822575854 * ___builder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MiniJSON::serializeNumber(System.Double,System.Text.StringBuilder)
extern "C"  void MiniJSON_serializeNumber_m3607534511 (Il2CppObject * __this /* static, unused */, double ___number, StringBuilder_t3822575854 * ___builder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
