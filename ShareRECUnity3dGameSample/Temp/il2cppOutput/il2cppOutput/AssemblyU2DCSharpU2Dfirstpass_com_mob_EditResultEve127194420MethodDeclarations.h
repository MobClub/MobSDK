﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// com.mob.EditResultEvent
struct EditResultEvent_t127194420;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void com.mob.EditResultEvent::.ctor(System.Object,System.IntPtr)
extern "C"  void EditResultEvent__ctor_m1698835619 (EditResultEvent_t127194420 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.EditResultEvent::Invoke(System.Boolean)
extern "C"  void EditResultEvent_Invoke_m956532084 (EditResultEvent_t127194420 * __this, bool ___cancelled, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_EditResultEvent_t127194420(Il2CppObject* delegate, bool ___cancelled);
// System.IAsyncResult com.mob.EditResultEvent::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * EditResultEvent_BeginInvoke_m3342741273 (EditResultEvent_t127194420 * __this, bool ___cancelled, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.EditResultEvent::EndInvoke(System.IAsyncResult)
extern "C"  void EditResultEvent_EndInvoke_m4170410035 (EditResultEvent_t127194420 * __this, Il2CppObject * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
