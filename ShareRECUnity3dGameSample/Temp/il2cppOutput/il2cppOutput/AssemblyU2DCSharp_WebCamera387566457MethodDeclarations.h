﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebCamera
struct WebCamera_t387566457;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void WebCamera::.ctor()
extern "C"  void WebCamera__ctor_m93069842 (WebCamera_t387566457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebCamera::Start()
extern "C"  void WebCamera_Start_m3335174930 (WebCamera_t387566457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebCamera::OnDestroy()
extern "C"  void WebCamera_OnDestroy_m3480919563 (WebCamera_t387566457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator WebCamera::GetCamera()
extern "C"  Il2CppObject * WebCamera_GetCamera_m306297683 (WebCamera_t387566457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebCamera::StartCamera()
extern "C"  void WebCamera_StartCamera_m866393175 (WebCamera_t387566457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebCamera::StopCamera()
extern "C"  void WebCamera_StopCamera_m3000787001 (WebCamera_t387566457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebCamera::HasFrontCamera()
extern "C"  bool WebCamera_HasFrontCamera_m689030962 (WebCamera_t387566457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebCamera::ChangeCamera()
extern "C"  void WebCamera_ChangeCamera_m1349637543 (WebCamera_t387566457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebCamera::SavePhoto(System.String)
extern "C"  void WebCamera_SavePhoto_m2894819837 (WebCamera_t387566457 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
