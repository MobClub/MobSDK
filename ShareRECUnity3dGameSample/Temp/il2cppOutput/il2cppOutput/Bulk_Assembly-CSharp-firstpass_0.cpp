﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// com.mob.CloseEvent
struct CloseEvent_t4246496483;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// com.mob.EditResultEvent
struct EditResultEvent_t127194420;
// com.mob.FinishedRecordEvent
struct FinishedRecordEvent_t4162477592;
// System.Exception
struct Exception_t1967233988;
// com.mob.ShareREC
struct ShareREC_t3659413906;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t3875263730;
// MiniJSON
struct MiniJSON_t2999478751;
// System.Char[]
struct CharU5BU5D_t3416858730;
// System.Collections.ArrayList
struct ArrayList_t2121638921;
// System.Text.StringBuilder
struct StringBuilder_t3822575854;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_CloseEvent4246496483.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_CloseEvent4246496483MethodDeclarations.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_EditResultEve127194420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_EditResultEve127194420MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_FinishedReco4162477592.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_FinishedReco4162477592MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_ShareREC3659413906.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_ShareREC3659413906MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_Application450040189MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_ShareRECIOS1702332221MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1574985880.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_Single958209021.h"
#include "mscorlib_System_Collections_Hashtable3875263730.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_SocialPageTyp127466071.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_ShareRECIOS1702332221.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MiniJSON2999478751MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988MethodDeclarations.h"
#include "mscorlib_System_Convert1097883944MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395.h"
#include "mscorlib_System_Collections_Hashtable3875263730MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_SocialPageTyp127466071MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MiniJSON2999478751.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Char2778706699.h"
#include "mscorlib_System_Text_StringBuilder3822575854MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder3822575854.h"
#include "mscorlib_System_Collections_ArrayList2121638921.h"
#include "mscorlib_System_Collections_ArrayList2121638921MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341MethodDeclarations.h"
#include "mscorlib_System_Double534516614.h"
#include "mscorlib_System_Array2840145358MethodDeclarations.h"
#include "mscorlib_System_UInt32985925326MethodDeclarations.h"
#include "mscorlib_System_Char2778706699MethodDeclarations.h"
#include "mscorlib_System_UInt32985925326.h"
#include "mscorlib_System_Globalization_NumberStyles3988678145.h"
#include "mscorlib_System_Double534516614MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2606186806.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2606186806MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2373214747MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22094718104MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22094718104.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2373214747.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "mscorlib_System_Decimal1688557254.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MiniJsonExtensions3346332531.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MiniJsonExtensions3346332531MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void com.mob.CloseEvent::.ctor(System.Object,System.IntPtr)
extern "C"  void CloseEvent__ctor_m2047460688 (CloseEvent_t4246496483 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->set_method_ptr_0((methodPointerType)((MethodInfo*)___method.get_m_value_0())->method);
	__this->set_method_3(___method);
	__this->set_m_target_2(___object);
}
// System.Void com.mob.CloseEvent::Invoke()
extern "C"  void CloseEvent_Invoke_m3525735850 (CloseEvent_t4246496483 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CloseEvent_Invoke_m3525735850((CloseEvent_t4246496483 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_CloseEvent_t4246496483(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult com.mob.CloseEvent::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CloseEvent_BeginInvoke_m953735449 (CloseEvent_t4246496483 * __this, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void com.mob.CloseEvent::EndInvoke(System.IAsyncResult)
extern "C"  void CloseEvent_EndInvoke_m1110728544 (CloseEvent_t4246496483 * __this, Il2CppObject * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void com.mob.EditResultEvent::.ctor(System.Object,System.IntPtr)
extern "C"  void EditResultEvent__ctor_m1698835619 (EditResultEvent_t127194420 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->set_method_ptr_0((methodPointerType)((MethodInfo*)___method.get_m_value_0())->method);
	__this->set_method_3(___method);
	__this->set_m_target_2(___object);
}
// System.Void com.mob.EditResultEvent::Invoke(System.Boolean)
extern "C"  void EditResultEvent_Invoke_m956532084 (EditResultEvent_t127194420 * __this, bool ___cancelled, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		EditResultEvent_Invoke_m956532084((EditResultEvent_t127194420 *)__this->get_prev_9(),___cancelled, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___cancelled, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___cancelled,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___cancelled, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___cancelled,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_EditResultEvent_t127194420(Il2CppObject* delegate, bool ___cancelled)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___cancelled' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___cancelled);

	// Marshaling cleanup of parameter '___cancelled' native representation

}
// System.IAsyncResult com.mob.EditResultEvent::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern TypeInfo* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t EditResultEvent_BeginInvoke_m3342741273_MetadataUsageId;
extern "C"  Il2CppObject * EditResultEvent_BeginInvoke_m3342741273 (EditResultEvent_t127194420 * __this, bool ___cancelled, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EditResultEvent_BeginInvoke_m3342741273_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___cancelled);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void com.mob.EditResultEvent::EndInvoke(System.IAsyncResult)
extern "C"  void EditResultEvent_EndInvoke_m4170410035 (EditResultEvent_t127194420 * __this, Il2CppObject * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void com.mob.FinishedRecordEvent::.ctor(System.Object,System.IntPtr)
extern "C"  void FinishedRecordEvent__ctor_m3923931911 (FinishedRecordEvent_t4162477592 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->set_method_ptr_0((methodPointerType)((MethodInfo*)___method.get_m_value_0())->method);
	__this->set_method_3(___method);
	__this->set_m_target_2(___object);
}
// System.Void com.mob.FinishedRecordEvent::Invoke(System.Exception)
extern "C"  void FinishedRecordEvent_Invoke_m3382741201 (FinishedRecordEvent_t4162477592 * __this, Exception_t1967233988 * ___ex, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FinishedRecordEvent_Invoke_m3382741201((FinishedRecordEvent_t4162477592 *)__this->get_prev_9(),___ex, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Exception_t1967233988 * ___ex, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___ex,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Exception_t1967233988 * ___ex, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___ex,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___ex,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_FinishedRecordEvent_t4162477592(Il2CppObject* delegate, Exception_t1967233988 * ___ex)
{
	// Marshaling of parameter '___ex' to native representation
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Exception'."));
}
// System.IAsyncResult com.mob.FinishedRecordEvent::BeginInvoke(System.Exception,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FinishedRecordEvent_BeginInvoke_m1902627620 (FinishedRecordEvent_t4162477592 * __this, Exception_t1967233988 * ___ex, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___ex;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void com.mob.FinishedRecordEvent::EndInvoke(System.IAsyncResult)
extern "C"  void FinishedRecordEvent_EndInvoke_m1392410775 (FinishedRecordEvent_t4162477592 * __this, Il2CppObject * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void com.mob.ShareREC::.ctor()
extern "C"  void ShareREC__ctor_m2604471299 (ShareREC_t3659413906 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void com.mob.ShareREC::shareRECCallback(System.String)
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareREC_shareRECCallback_m2793148811_MetadataUsageId;
extern "C"  void ShareREC_shareRECCallback_m2793148811 (ShareREC_t3659413906 * __this, String_t* ___data, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareREC_shareRECCallback_m2793148811_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0016;
		}
	}
	{
		String_t* L_1 = ___data;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		ShareRECIOS_shareRECCallback_m2673680446(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		goto IL_0022;
	}

IL_0016:
	{
		int32_t L_2 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0022;
		}
	}

IL_0022:
	{
		return;
	}
}
// System.Void com.mob.ShareREC::setCallbackObjectName(System.String)
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3759572844;
extern const uint32_t ShareREC_setCallbackObjectName_m1196151472_MetadataUsageId;
extern "C"  void ShareREC_setCallbackObjectName_m1196151472 (Il2CppObject * __this /* static, unused */, String_t* ___objectName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareREC_setCallbackObjectName_m1196151472_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___objectName;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		___objectName = _stringLiteral3759572844;
	}

IL_000d:
	{
		int32_t L_1 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)8))))
		{
			goto IL_0023;
		}
	}
	{
		String_t* L_2 = ___objectName;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		ShareRECIOS_setCallbackObjectName_m3361143773(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		goto IL_002f;
	}

IL_0023:
	{
		int32_t L_3 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_002f;
		}
	}

IL_002f:
	{
		return;
	}
}
// System.Void com.mob.ShareREC::registerApp(System.String)
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareREC_registerApp_m104679203_MetadataUsageId;
extern "C"  void ShareREC_registerApp_m104679203 (Il2CppObject * __this /* static, unused */, String_t* ___appKey, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareREC_registerApp_m104679203_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___appKey;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		ShareRECIOS_registerApp_m1021613200(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void com.mob.ShareREC::startRecoring()
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareREC_startRecoring_m2705032210_MetadataUsageId;
extern "C"  void ShareREC_startRecoring_m2705032210 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareREC_startRecoring_m2705032210_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		ShareRECIOS_startRecording_m461515357(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void com.mob.ShareREC::stopRecording(com.mob.FinishedRecordEvent)
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareREC_stopRecording_m2331859558_MetadataUsageId;
extern "C"  void ShareREC_stopRecording_m2331859558 (Il2CppObject * __this /* static, unused */, FinishedRecordEvent_t4162477592 * ___evt, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareREC_stopRecording_m2331859558_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FinishedRecordEvent_t4162477592 * L_0 = ___evt;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		ShareRECIOS_stopRecording_m62795219(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void com.mob.ShareREC::playLastRecording()
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareREC_playLastRecording_m2258034856_MetadataUsageId;
extern "C"  void ShareREC_playLastRecording_m2258034856 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareREC_playLastRecording_m2258034856_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		ShareRECIOS_playLastRecording_m4113179611(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void com.mob.ShareREC::setBitRate(System.Int32)
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareREC_setBitRate_m3609732637_MetadataUsageId;
extern "C"  void ShareREC_setBitRate_m3609732637 (Il2CppObject * __this /* static, unused */, int32_t ___bitRate, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareREC_setBitRate_m3609732637_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___bitRate;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		ShareRECIOS_setBitRate_m942533322(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void com.mob.ShareREC::setFPS(System.Int32)
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareREC_setFPS_m4062387577_MetadataUsageId;
extern "C"  void ShareREC_setFPS_m4062387577 (Il2CppObject * __this /* static, unused */, int32_t ___fps, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareREC_setFPS_m4062387577_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___fps;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		ShareRECIOS_setFPS_m1442332838(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void com.mob.ShareREC::setMinimumRecordingTime(System.Single)
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareREC_setMinimumRecordingTime_m2717129048_MetadataUsageId;
extern "C"  void ShareREC_setMinimumRecordingTime_m2717129048 (Il2CppObject * __this /* static, unused */, float ___time, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareREC_setMinimumRecordingTime_m2717129048_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___time;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		ShareRECIOS_setMinimumRecordingTime_m215591749(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String com.mob.ShareREC::lastRecordingPath()
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareREC_lastRecordingPath_m2454543038_MetadataUsageId;
extern "C"  String_t* ShareREC_lastRecordingPath_m2454543038 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareREC_lastRecordingPath_m2454543038_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		String_t* L_0 = ShareRECIOS_lastRecordingPath_m130556759(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void com.mob.ShareREC::editLastingRecording(System.String,System.Collections.Hashtable,com.mob.CloseEvent)
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareREC_editLastingRecording_m3284923037_MetadataUsageId;
extern "C"  void ShareREC_editLastingRecording_m3284923037 (Il2CppObject * __this /* static, unused */, String_t* ___title, Hashtable_t3875263730 * ___userData, CloseEvent_t4246496483 * ___evt, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareREC_editLastingRecording_m3284923037_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___title;
		Hashtable_t3875263730 * L_1 = ___userData;
		CloseEvent_t4246496483 * L_2 = ___evt;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		ShareRECIOS_editLastRecording_m4004253032(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void com.mob.ShareREC::editLastRecording(com.mob.EditResultEvent)
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareREC_editLastRecording_m942934280_MetadataUsageId;
extern "C"  void ShareREC_editLastRecording_m942934280 (Il2CppObject * __this /* static, unused */, EditResultEvent_t127194420 * ___evt, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareREC_editLastRecording_m942934280_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EditResultEvent_t127194420 * L_0 = ___evt;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		ShareRECIOS_editLastRecording_m2968837237(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void com.mob.ShareREC::setSyncAudioComment(System.Boolean)
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareREC_setSyncAudioComment_m3360636254_MetadataUsageId;
extern "C"  void ShareREC_setSyncAudioComment_m3360636254 (Il2CppObject * __this /* static, unused */, bool ___flag, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareREC_setSyncAudioComment_m3360636254_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___flag;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		ShareRECIOS_setSyncAudioComment_m936622737(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void com.mob.ShareREC::openSocial(System.String,System.Collections.Hashtable,com.mob.SocialPageType,com.mob.CloseEvent)
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareREC_openSocial_m2040620200_MetadataUsageId;
extern "C"  void ShareREC_openSocial_m2040620200 (Il2CppObject * __this /* static, unused */, String_t* ___title, Hashtable_t3875263730 * ___userData, int32_t ___pageType, CloseEvent_t4246496483 * ___evt, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareREC_openSocial_m2040620200_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___title;
		Hashtable_t3875263730 * L_1 = ___userData;
		int32_t L_2 = ___pageType;
		CloseEvent_t4246496483 * L_3 = ___evt;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		ShareRECIOS_openSocial_m4253959253(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void com.mob.ShareRECIOS::.cctor()
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3759572844;
extern const uint32_t ShareRECIOS__cctor_m656406199_MetadataUsageId;
extern "C"  void ShareRECIOS__cctor_m656406199 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareRECIOS__cctor_m656406199_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((ShareRECIOS_t1702332221_StaticFields*)ShareRECIOS_t1702332221_il2cpp_TypeInfo_var->static_fields)->set__callbackObjectName_0(_stringLiteral3759572844);
		return;
	}
}
// System.Void com.mob.ShareRECIOS::__iosShareRECRegisterApp(System.String)
extern "C" {void DEFAULT_CALL __iosShareRECRegisterApp(char*);}
extern "C"  void ShareRECIOS___iosShareRECRegisterApp_m1552351604 (Il2CppObject * __this /* static, unused */, String_t* ___appKey, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)__iosShareRECRegisterApp;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: '__iosShareRECRegisterApp'"));
		}
	}

	// Marshaling of parameter '___appKey' to native representation
	char* ____appKey_marshaled = NULL;
	____appKey_marshaled = il2cpp_codegen_marshal_string(___appKey);

	// Native function invocation
	_il2cpp_pinvoke_func(____appKey_marshaled);

	// Marshaling cleanup of parameter '___appKey' native representation
	il2cpp_codegen_marshal_free(____appKey_marshaled);
	____appKey_marshaled = NULL;

}
// System.Void com.mob.ShareRECIOS::__iosShareRECStartRecording()
extern "C" {void DEFAULT_CALL __iosShareRECStartRecording();}
extern "C"  void ShareRECIOS___iosShareRECStartRecording_m1115202625 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)__iosShareRECStartRecording;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: '__iosShareRECStartRecording'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Void com.mob.ShareRECIOS::__iosShareRECStopRecording(System.String)
extern "C" {void DEFAULT_CALL __iosShareRECStopRecording(char*);}
extern "C"  void ShareRECIOS___iosShareRECStopRecording_m2450047363 (Il2CppObject * __this /* static, unused */, String_t* ___observer, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)__iosShareRECStopRecording;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: '__iosShareRECStopRecording'"));
		}
	}

	// Marshaling of parameter '___observer' to native representation
	char* ____observer_marshaled = NULL;
	____observer_marshaled = il2cpp_codegen_marshal_string(___observer);

	// Native function invocation
	_il2cpp_pinvoke_func(____observer_marshaled);

	// Marshaling cleanup of parameter '___observer' native representation
	il2cpp_codegen_marshal_free(____observer_marshaled);
	____observer_marshaled = NULL;

}
// System.Void com.mob.ShareRECIOS::__iosShareRECPlayLastRecording()
extern "C" {void DEFAULT_CALL __iosShareRECPlayLastRecording();}
extern "C"  void ShareRECIOS___iosShareRECPlayLastRecording_m433893239 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)__iosShareRECPlayLastRecording;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: '__iosShareRECPlayLastRecording'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Void com.mob.ShareRECIOS::__iosShareRECSetBitRate(System.Int32)
extern "C" {void DEFAULT_CALL __iosShareRECSetBitRate(int32_t);}
extern "C"  void ShareRECIOS___iosShareRECSetBitRate_m1854816430 (Il2CppObject * __this /* static, unused */, int32_t ___bitRate, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)__iosShareRECSetBitRate;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: '__iosShareRECSetBitRate'"));
		}
	}

	// Marshaling of parameter '___bitRate' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___bitRate);

	// Marshaling cleanup of parameter '___bitRate' native representation

}
// System.Void com.mob.ShareRECIOS::__iosShareRECSetFPS(System.Int32)
extern "C" {void DEFAULT_CALL __iosShareRECSetFPS(int32_t);}
extern "C"  void ShareRECIOS___iosShareRECSetFPS_m3348572298 (Il2CppObject * __this /* static, unused */, int32_t ___fps, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)__iosShareRECSetFPS;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: '__iosShareRECSetFPS'"));
		}
	}

	// Marshaling of parameter '___fps' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___fps);

	// Marshaling cleanup of parameter '___fps' native representation

}
// System.String com.mob.ShareRECIOS::__iosShareRECLastRecordingPath()
extern "C" {char* DEFAULT_CALL __iosShareRECLastRecordingPath();}
extern "C"  String_t* ShareRECIOS___iosShareRECLastRecordingPath_m3348631437 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)__iosShareRECLastRecordingPath;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: '__iosShareRECLastRecordingPath'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = _il2cpp_pinvoke_func();
	String_t* __return_value_unmarshaled = NULL;
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	return __return_value_unmarshaled;
}
// System.Void com.mob.ShareRECIOS::__iosShareRECEditLastRecording(System.String,System.String,System.String)
extern "C" {void DEFAULT_CALL __iosShareRECEditLastRecording(char*, char*, char*);}
extern "C"  void ShareRECIOS___iosShareRECEditLastRecording_m1045588665 (Il2CppObject * __this /* static, unused */, String_t* ___title, String_t* ___userData, String_t* ___observer, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)__iosShareRECEditLastRecording;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: '__iosShareRECEditLastRecording'"));
		}
	}

	// Marshaling of parameter '___title' to native representation
	char* ____title_marshaled = NULL;
	____title_marshaled = il2cpp_codegen_marshal_string(___title);

	// Marshaling of parameter '___userData' to native representation
	char* ____userData_marshaled = NULL;
	____userData_marshaled = il2cpp_codegen_marshal_string(___userData);

	// Marshaling of parameter '___observer' to native representation
	char* ____observer_marshaled = NULL;
	____observer_marshaled = il2cpp_codegen_marshal_string(___observer);

	// Native function invocation
	_il2cpp_pinvoke_func(____title_marshaled, ____userData_marshaled, ____observer_marshaled);

	// Marshaling cleanup of parameter '___title' native representation
	il2cpp_codegen_marshal_free(____title_marshaled);
	____title_marshaled = NULL;

	// Marshaling cleanup of parameter '___userData' native representation
	il2cpp_codegen_marshal_free(____userData_marshaled);
	____userData_marshaled = NULL;

	// Marshaling cleanup of parameter '___observer' native representation
	il2cpp_codegen_marshal_free(____observer_marshaled);
	____observer_marshaled = NULL;

}
// System.Void com.mob.ShareRECIOS::__iosShareRECEditLastRecordingNew(System.String)
extern "C" {void DEFAULT_CALL __iosShareRECEditLastRecordingNew(char*);}
extern "C"  void ShareRECIOS___iosShareRECEditLastRecordingNew_m1854432001 (Il2CppObject * __this /* static, unused */, String_t* ___observer, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)__iosShareRECEditLastRecordingNew;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: '__iosShareRECEditLastRecordingNew'"));
		}
	}

	// Marshaling of parameter '___observer' to native representation
	char* ____observer_marshaled = NULL;
	____observer_marshaled = il2cpp_codegen_marshal_string(___observer);

	// Native function invocation
	_il2cpp_pinvoke_func(____observer_marshaled);

	// Marshaling cleanup of parameter '___observer' native representation
	il2cpp_codegen_marshal_free(____observer_marshaled);
	____observer_marshaled = NULL;

}
// System.Void com.mob.ShareRECIOS::__iosShareRECSocialOpen(System.String,System.String,System.Int32,System.String)
extern "C" {void DEFAULT_CALL __iosShareRECSocialOpen(char*, char*, int32_t, char*);}
extern "C"  void ShareRECIOS___iosShareRECSocialOpen_m2750311422 (Il2CppObject * __this /* static, unused */, String_t* ___title, String_t* ___userData, int32_t ___pageType, String_t* ___observer, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, int32_t, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)__iosShareRECSocialOpen;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: '__iosShareRECSocialOpen'"));
		}
	}

	// Marshaling of parameter '___title' to native representation
	char* ____title_marshaled = NULL;
	____title_marshaled = il2cpp_codegen_marshal_string(___title);

	// Marshaling of parameter '___userData' to native representation
	char* ____userData_marshaled = NULL;
	____userData_marshaled = il2cpp_codegen_marshal_string(___userData);

	// Marshaling of parameter '___pageType' to native representation

	// Marshaling of parameter '___observer' to native representation
	char* ____observer_marshaled = NULL;
	____observer_marshaled = il2cpp_codegen_marshal_string(___observer);

	// Native function invocation
	_il2cpp_pinvoke_func(____title_marshaled, ____userData_marshaled, ___pageType, ____observer_marshaled);

	// Marshaling cleanup of parameter '___title' native representation
	il2cpp_codegen_marshal_free(____title_marshaled);
	____title_marshaled = NULL;

	// Marshaling cleanup of parameter '___userData' native representation
	il2cpp_codegen_marshal_free(____userData_marshaled);
	____userData_marshaled = NULL;

	// Marshaling cleanup of parameter '___pageType' native representation

	// Marshaling cleanup of parameter '___observer' native representation
	il2cpp_codegen_marshal_free(____observer_marshaled);
	____observer_marshaled = NULL;

}
// System.Void com.mob.ShareRECIOS::__iosShareRECSetMinimumRecordingTime(System.Single)
extern "C" {void DEFAULT_CALL __iosShareRECSetMinimumRecordingTime(float);}
extern "C"  void ShareRECIOS___iosShareRECSetMinimumRecordingTime_m2095833641 (Il2CppObject * __this /* static, unused */, float ___time, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (float);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)__iosShareRECSetMinimumRecordingTime;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: '__iosShareRECSetMinimumRecordingTime'"));
		}
	}

	// Marshaling of parameter '___time' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___time);

	// Marshaling cleanup of parameter '___time' native representation

}
// System.Void com.mob.ShareRECIOS::__iosShareRECSetSyncAudioComment(System.Boolean)
extern "C" {void DEFAULT_CALL __iosShareRECSetSyncAudioComment(int32_t);}
extern "C"  void ShareRECIOS___iosShareRECSetSyncAudioComment_m462655021 (Il2CppObject * __this /* static, unused */, bool ___syncAudioComment, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)__iosShareRECSetSyncAudioComment;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: '__iosShareRECSetSyncAudioComment'"));
		}
	}

	// Marshaling of parameter '___syncAudioComment' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___syncAudioComment);

	// Marshaling cleanup of parameter '___syncAudioComment' native representation

}
// System.Void com.mob.ShareRECIOS::setCallbackObjectName(System.String)
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareRECIOS_setCallbackObjectName_m3361143773_MetadataUsageId;
extern "C"  void ShareRECIOS_setCallbackObjectName_m3361143773 (Il2CppObject * __this /* static, unused */, String_t* ___objectName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareRECIOS_setCallbackObjectName_m3361143773_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___objectName;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		((ShareRECIOS_t1702332221_StaticFields*)ShareRECIOS_t1702332221_il2cpp_TypeInfo_var->static_fields)->set__callbackObjectName_0(L_0);
		return;
	}
}
// System.Void com.mob.ShareRECIOS::shareRECCallback(System.String)
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern TypeInfo* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t190145395_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1097883944_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral2146397569;
extern Il2CppCodeGenString* _stringLiteral3946519147;
extern Il2CppCodeGenString* _stringLiteral3890728679;
extern Il2CppCodeGenString* _stringLiteral96784904;
extern Il2CppCodeGenString* _stringLiteral954925063;
extern Il2CppCodeGenString* _stringLiteral476588369;
extern const uint32_t ShareRECIOS_shareRECCallback_m2673680446_MetadataUsageId;
extern "C"  void ShareRECIOS_shareRECCallback_m2673680446 (Il2CppObject * __this /* static, unused */, String_t* ___data, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareRECIOS_shareRECCallback_m2673680446_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Hashtable_t3875263730 * V_1 = NULL;
	String_t* V_2 = NULL;
	Exception_t1967233988 * V_3 = NULL;
	String_t* V_4 = NULL;
	Hashtable_t3875263730 * V_5 = NULL;
	bool V_6 = false;
	String_t* V_7 = NULL;
	Dictionary_2_t190145395 * V_8 = NULL;
	int32_t V_9 = 0;
	{
		String_t* L_0 = ___data;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = MiniJSON_jsonDecode_m2239788899(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Il2CppObject * L_2 = V_0;
		if (!((Hashtable_t3875263730 *)IsInstClass(L_2, Hashtable_t3875263730_il2cpp_TypeInfo_var)))
		{
			goto IL_017b;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		V_1 = ((Hashtable_t3875263730 *)IsInstClass(L_3, Hashtable_t3875263730_il2cpp_TypeInfo_var));
		Hashtable_t3875263730 * L_4 = V_1;
		if (!L_4)
		{
			goto IL_017b;
		}
	}
	{
		Hashtable_t3875263730 * L_5 = V_1;
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(31 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_5, _stringLiteral3373707);
		if (!L_6)
		{
			goto IL_017b;
		}
	}
	{
		Hashtable_t3875263730 * L_7 = V_1;
		NullCheck(L_7);
		Il2CppObject * L_8 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(23 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_7, _stringLiteral3373707);
		V_2 = ((String_t*)IsInstSealed(L_8, String_t_il2cpp_TypeInfo_var));
		String_t* L_9 = V_2;
		V_7 = L_9;
		String_t* L_10 = V_7;
		if (!L_10)
		{
			goto IL_017b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		Dictionary_2_t190145395 * L_11 = ((ShareRECIOS_t1702332221_StaticFields*)ShareRECIOS_t1702332221_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_4();
		if (L_11)
		{
			goto IL_008a;
		}
	}
	{
		Dictionary_2_t190145395 * L_12 = (Dictionary_2_t190145395 *)il2cpp_codegen_object_new(Dictionary_2_t190145395_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1958628151(L_12, 3, /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
		V_8 = L_12;
		Dictionary_2_t190145395 * L_13 = V_8;
		NullCheck(L_13);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_13, _stringLiteral2146397569, 0);
		Dictionary_2_t190145395 * L_14 = V_8;
		NullCheck(L_14);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_14, _stringLiteral3946519147, 1);
		Dictionary_2_t190145395 * L_15 = V_8;
		NullCheck(L_15);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_15, _stringLiteral3890728679, 2);
		Dictionary_2_t190145395 * L_16 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		((ShareRECIOS_t1702332221_StaticFields*)ShareRECIOS_t1702332221_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map0_4(L_16);
	}

IL_008a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		Dictionary_2_t190145395 * L_17 = ((ShareRECIOS_t1702332221_StaticFields*)ShareRECIOS_t1702332221_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_4();
		String_t* L_18 = V_7;
		NullCheck(L_17);
		bool L_19 = VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_17, L_18, (&V_9));
		if (!L_19)
		{
			goto IL_017b;
		}
	}
	{
		int32_t L_20 = V_9;
		if (L_20 == 0)
		{
			goto IL_00b5;
		}
		if (L_20 == 1)
		{
			goto IL_0122;
		}
		if (L_20 == 2)
		{
			goto IL_013b;
		}
	}
	{
		goto IL_017b;
	}

IL_00b5:
	{
		V_3 = (Exception_t1967233988 *)NULL;
		Hashtable_t3875263730 * L_21 = V_1;
		NullCheck(L_21);
		bool L_22 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(31 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_21, _stringLiteral96784904);
		if (!L_22)
		{
			goto IL_0108;
		}
	}
	{
		V_4 = (String_t*)NULL;
		Hashtable_t3875263730 * L_23 = V_1;
		NullCheck(L_23);
		Il2CppObject * L_24 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(23 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_23, _stringLiteral96784904);
		V_5 = ((Hashtable_t3875263730 *)IsInstClass(L_24, Hashtable_t3875263730_il2cpp_TypeInfo_var));
		Hashtable_t3875263730 * L_25 = V_5;
		NullCheck(L_25);
		bool L_26 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(31 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_25, _stringLiteral954925063);
		if (!L_26)
		{
			goto IL_0100;
		}
	}
	{
		Hashtable_t3875263730 * L_27 = V_5;
		NullCheck(L_27);
		Il2CppObject * L_28 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(23 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_27, _stringLiteral954925063);
		V_4 = ((String_t*)IsInstSealed(L_28, String_t_il2cpp_TypeInfo_var));
	}

IL_0100:
	{
		String_t* L_29 = V_4;
		Exception_t1967233988 * L_30 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_30, L_29, /*hidden argument*/NULL);
		V_3 = L_30;
	}

IL_0108:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		FinishedRecordEvent_t4162477592 * L_31 = ((ShareRECIOS_t1702332221_StaticFields*)ShareRECIOS_t1702332221_il2cpp_TypeInfo_var->static_fields)->get__finishedRecordHandler_1();
		if (!L_31)
		{
			goto IL_011d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		FinishedRecordEvent_t4162477592 * L_32 = ((ShareRECIOS_t1702332221_StaticFields*)ShareRECIOS_t1702332221_il2cpp_TypeInfo_var->static_fields)->get__finishedRecordHandler_1();
		Exception_t1967233988 * L_33 = V_3;
		NullCheck(L_32);
		FinishedRecordEvent_Invoke_m3382741201(L_32, L_33, /*hidden argument*/NULL);
	}

IL_011d:
	{
		goto IL_017b;
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		CloseEvent_t4246496483 * L_34 = ((ShareRECIOS_t1702332221_StaticFields*)ShareRECIOS_t1702332221_il2cpp_TypeInfo_var->static_fields)->get__closeHandler_2();
		if (!L_34)
		{
			goto IL_0136;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		CloseEvent_t4246496483 * L_35 = ((ShareRECIOS_t1702332221_StaticFields*)ShareRECIOS_t1702332221_il2cpp_TypeInfo_var->static_fields)->get__closeHandler_2();
		NullCheck(L_35);
		CloseEvent_Invoke_m3525735850(L_35, /*hidden argument*/NULL);
	}

IL_0136:
	{
		goto IL_017b;
	}

IL_013b:
	{
		V_6 = (bool)0;
		Hashtable_t3875263730 * L_36 = V_1;
		NullCheck(L_36);
		bool L_37 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(31 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_36, _stringLiteral476588369);
		if (!L_37)
		{
			goto IL_0160;
		}
	}
	{
		Hashtable_t3875263730 * L_38 = V_1;
		NullCheck(L_38);
		Il2CppObject * L_39 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(23 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_38, _stringLiteral476588369);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		bool L_40 = Convert_ToBoolean_m531114797(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		V_6 = L_40;
	}

IL_0160:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		EditResultEvent_t127194420 * L_41 = ((ShareRECIOS_t1702332221_StaticFields*)ShareRECIOS_t1702332221_il2cpp_TypeInfo_var->static_fields)->get__editResultHandler_3();
		if (!L_41)
		{
			goto IL_0176;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		EditResultEvent_t127194420 * L_42 = ((ShareRECIOS_t1702332221_StaticFields*)ShareRECIOS_t1702332221_il2cpp_TypeInfo_var->static_fields)->get__editResultHandler_3();
		bool L_43 = V_6;
		NullCheck(L_42);
		EditResultEvent_Invoke_m956532084(L_42, L_43, /*hidden argument*/NULL);
	}

IL_0176:
	{
		goto IL_017b;
	}

IL_017b:
	{
		return;
	}
}
// System.Void com.mob.ShareRECIOS::registerApp(System.String)
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareRECIOS_registerApp_m1021613200_MetadataUsageId;
extern "C"  void ShareRECIOS_registerApp_m1021613200 (Il2CppObject * __this /* static, unused */, String_t* ___appKey, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareRECIOS_registerApp_m1021613200_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___appKey;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		ShareRECIOS___iosShareRECRegisterApp_m1552351604(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void com.mob.ShareRECIOS::startRecording()
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareRECIOS_startRecording_m461515357_MetadataUsageId;
extern "C"  void ShareRECIOS_startRecording_m461515357 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareRECIOS_startRecording_m461515357_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		ShareRECIOS___iosShareRECStartRecording_m1115202625(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void com.mob.ShareRECIOS::stopRecording(com.mob.FinishedRecordEvent)
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareRECIOS_stopRecording_m62795219_MetadataUsageId;
extern "C"  void ShareRECIOS_stopRecording_m62795219 (Il2CppObject * __this /* static, unused */, FinishedRecordEvent_t4162477592 * ___evt, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareRECIOS_stopRecording_m62795219_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FinishedRecordEvent_t4162477592 * L_0 = ___evt;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		((ShareRECIOS_t1702332221_StaticFields*)ShareRECIOS_t1702332221_il2cpp_TypeInfo_var->static_fields)->set__finishedRecordHandler_1(L_0);
		String_t* L_1 = ((ShareRECIOS_t1702332221_StaticFields*)ShareRECIOS_t1702332221_il2cpp_TypeInfo_var->static_fields)->get__callbackObjectName_0();
		ShareRECIOS___iosShareRECStopRecording_m2450047363(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void com.mob.ShareRECIOS::playLastRecording()
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareRECIOS_playLastRecording_m4113179611_MetadataUsageId;
extern "C"  void ShareRECIOS_playLastRecording_m4113179611 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareRECIOS_playLastRecording_m4113179611_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		ShareRECIOS___iosShareRECPlayLastRecording_m433893239(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void com.mob.ShareRECIOS::setBitRate(System.Int32)
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareRECIOS_setBitRate_m942533322_MetadataUsageId;
extern "C"  void ShareRECIOS_setBitRate_m942533322 (Il2CppObject * __this /* static, unused */, int32_t ___bitRate, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareRECIOS_setBitRate_m942533322_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___bitRate;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		ShareRECIOS___iosShareRECSetBitRate_m1854816430(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void com.mob.ShareRECIOS::setFPS(System.Int32)
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareRECIOS_setFPS_m1442332838_MetadataUsageId;
extern "C"  void ShareRECIOS_setFPS_m1442332838 (Il2CppObject * __this /* static, unused */, int32_t ___fps, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareRECIOS_setFPS_m1442332838_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___fps;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		ShareRECIOS___iosShareRECSetFPS_m3348572298(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void com.mob.ShareRECIOS::setMinimumRecordingTime(System.Single)
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareRECIOS_setMinimumRecordingTime_m215591749_MetadataUsageId;
extern "C"  void ShareRECIOS_setMinimumRecordingTime_m215591749 (Il2CppObject * __this /* static, unused */, float ___time, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareRECIOS_setMinimumRecordingTime_m215591749_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___time;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		ShareRECIOS___iosShareRECSetMinimumRecordingTime_m2095833641(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String com.mob.ShareRECIOS::lastRecordingPath()
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareRECIOS_lastRecordingPath_m130556759_MetadataUsageId;
extern "C"  String_t* ShareRECIOS_lastRecordingPath_m130556759 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareRECIOS_lastRecordingPath_m130556759_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		String_t* L_0 = ShareRECIOS___iosShareRECLastRecordingPath_m3348631437(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void com.mob.ShareRECIOS::editLastRecording(System.String,System.Collections.Hashtable,com.mob.CloseEvent)
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern const uint32_t ShareRECIOS_editLastRecording_m4004253032_MetadataUsageId;
extern "C"  void ShareRECIOS_editLastRecording_m4004253032 (Il2CppObject * __this /* static, unused */, String_t* ___title, Hashtable_t3875263730 * ___userData, CloseEvent_t4246496483 * ___evt, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareRECIOS_editLastRecording_m4004253032_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		CloseEvent_t4246496483 * L_0 = ___evt;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		((ShareRECIOS_t1702332221_StaticFields*)ShareRECIOS_t1702332221_il2cpp_TypeInfo_var->static_fields)->set__closeHandler_2(L_0);
		V_0 = (String_t*)NULL;
		Hashtable_t3875263730 * L_1 = ___userData;
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		Hashtable_t3875263730 * L_2 = ___userData;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		String_t* L_3 = MiniJSON_jsonEncode_m3503903903(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0015:
	{
		String_t* L_4 = ___title;
		String_t* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		String_t* L_6 = ((ShareRECIOS_t1702332221_StaticFields*)ShareRECIOS_t1702332221_il2cpp_TypeInfo_var->static_fields)->get__callbackObjectName_0();
		ShareRECIOS___iosShareRECEditLastRecording_m1045588665(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void com.mob.ShareRECIOS::editLastRecording(com.mob.EditResultEvent)
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareRECIOS_editLastRecording_m2968837237_MetadataUsageId;
extern "C"  void ShareRECIOS_editLastRecording_m2968837237 (Il2CppObject * __this /* static, unused */, EditResultEvent_t127194420 * ___evt, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareRECIOS_editLastRecording_m2968837237_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EditResultEvent_t127194420 * L_0 = ___evt;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		((ShareRECIOS_t1702332221_StaticFields*)ShareRECIOS_t1702332221_il2cpp_TypeInfo_var->static_fields)->set__editResultHandler_3(L_0);
		String_t* L_1 = ((ShareRECIOS_t1702332221_StaticFields*)ShareRECIOS_t1702332221_il2cpp_TypeInfo_var->static_fields)->get__callbackObjectName_0();
		ShareRECIOS___iosShareRECEditLastRecordingNew_m1854432001(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void com.mob.ShareRECIOS::setSyncAudioComment(System.Boolean)
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern const uint32_t ShareRECIOS_setSyncAudioComment_m936622737_MetadataUsageId;
extern "C"  void ShareRECIOS_setSyncAudioComment_m936622737 (Il2CppObject * __this /* static, unused */, bool ___flag, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareRECIOS_setSyncAudioComment_m936622737_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___flag;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		ShareRECIOS___iosShareRECSetSyncAudioComment_m462655021(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void com.mob.ShareRECIOS::openSocial(System.String,System.Collections.Hashtable,com.mob.SocialPageType,com.mob.CloseEvent)
extern TypeInfo* ShareRECIOS_t1702332221_il2cpp_TypeInfo_var;
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern const uint32_t ShareRECIOS_openSocial_m4253959253_MetadataUsageId;
extern "C"  void ShareRECIOS_openSocial_m4253959253 (Il2CppObject * __this /* static, unused */, String_t* ___title, Hashtable_t3875263730 * ___userData, int32_t ___pageType, CloseEvent_t4246496483 * ___evt, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShareRECIOS_openSocial_m4253959253_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		CloseEvent_t4246496483 * L_0 = ___evt;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		((ShareRECIOS_t1702332221_StaticFields*)ShareRECIOS_t1702332221_il2cpp_TypeInfo_var->static_fields)->set__closeHandler_2(L_0);
		V_0 = (String_t*)NULL;
		Hashtable_t3875263730 * L_1 = ___userData;
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		Hashtable_t3875263730 * L_2 = ___userData;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		String_t* L_3 = MiniJSON_jsonEncode_m3503903903(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0015:
	{
		String_t* L_4 = ___title;
		String_t* L_5 = V_0;
		int32_t L_6 = ___pageType;
		IL2CPP_RUNTIME_CLASS_INIT(ShareRECIOS_t1702332221_il2cpp_TypeInfo_var);
		String_t* L_7 = ((ShareRECIOS_t1702332221_StaticFields*)ShareRECIOS_t1702332221_il2cpp_TypeInfo_var->static_fields)->get__callbackObjectName_0();
		ShareRECIOS___iosShareRECSocialOpen_m2750311422(NULL /*static, unused*/, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MiniJSON::.ctor()
extern "C"  void MiniJSON__ctor_m3723319888 (MiniJSON_t2999478751 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MiniJSON::.cctor()
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON__cctor_m3271670621_MetadataUsageId;
extern "C"  void MiniJSON__cctor_m3271670621 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON__cctor_m3271670621_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((MiniJSON_t2999478751_StaticFields*)MiniJSON_t2999478751_il2cpp_TypeInfo_var->static_fields)->set_lastErrorIndex_13((-1));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		((MiniJSON_t2999478751_StaticFields*)MiniJSON_t2999478751_il2cpp_TypeInfo_var->static_fields)->set_lastDecode_14(L_0);
		return;
	}
}
// System.Object MiniJSON::jsonDecode(System.String)
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_jsonDecode_m2239788899_MetadataUsageId;
extern "C"  Il2CppObject * MiniJSON_jsonDecode_m2239788899 (Il2CppObject * __this /* static, unused */, String_t* ___json, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_jsonDecode_m2239788899_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CharU5BU5D_t3416858730* V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	Il2CppObject * V_3 = NULL;
	{
		String_t* L_0 = ___json;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		((MiniJSON_t2999478751_StaticFields*)MiniJSON_t2999478751_il2cpp_TypeInfo_var->static_fields)->set_lastDecode_14(L_0);
		String_t* L_1 = ___json;
		if (!L_1)
		{
			goto IL_003b;
		}
	}
	{
		String_t* L_2 = ___json;
		NullCheck(L_2);
		CharU5BU5D_t3416858730* L_3 = String_ToCharArray_m1208288742(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		V_2 = (bool)1;
		CharU5BU5D_t3416858730* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		Il2CppObject * L_5 = MiniJSON_parseValue_m1751092568(NULL /*static, unused*/, L_4, (&V_1), (&V_2), /*hidden argument*/NULL);
		V_3 = L_5;
		bool L_6 = V_2;
		if (!L_6)
		{
			goto IL_0033;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		((MiniJSON_t2999478751_StaticFields*)MiniJSON_t2999478751_il2cpp_TypeInfo_var->static_fields)->set_lastErrorIndex_13((-1));
		goto IL_0039;
	}

IL_0033:
	{
		int32_t L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		((MiniJSON_t2999478751_StaticFields*)MiniJSON_t2999478751_il2cpp_TypeInfo_var->static_fields)->set_lastErrorIndex_13(L_7);
	}

IL_0039:
	{
		Il2CppObject * L_8 = V_3;
		return L_8;
	}

IL_003b:
	{
		return NULL;
	}
}
// System.String MiniJSON::jsonEncode(System.Object)
extern TypeInfo* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_jsonEncode_m3503903903_MetadataUsageId;
extern "C"  String_t* MiniJSON_jsonEncode_m3503903903 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___json, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_jsonEncode_m3503903903_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t3822575854 * V_0 = NULL;
	bool V_1 = false;
	String_t* G_B3_0 = NULL;
	{
		StringBuilder_t3822575854 * L_0 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3624398269(L_0, ((int32_t)2000), /*hidden argument*/NULL);
		V_0 = L_0;
		Il2CppObject * L_1 = ___json;
		StringBuilder_t3822575854 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		bool L_3 = MiniJSON_serializeValue_m1454356919(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0024;
		}
	}
	{
		StringBuilder_t3822575854 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = StringBuilder_ToString_m350379841(L_5, /*hidden argument*/NULL);
		G_B3_0 = L_6;
		goto IL_0025;
	}

IL_0024:
	{
		G_B3_0 = ((String_t*)(NULL));
	}

IL_0025:
	{
		return G_B3_0;
	}
}
// System.Boolean MiniJSON::lastDecodeSuccessful()
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_lastDecodeSuccessful_m4224849614_MetadataUsageId;
extern "C"  bool MiniJSON_lastDecodeSuccessful_m4224849614 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_lastDecodeSuccessful_m4224849614_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		int32_t L_0 = ((MiniJSON_t2999478751_StaticFields*)MiniJSON_t2999478751_il2cpp_TypeInfo_var->static_fields)->get_lastErrorIndex_13();
		return (bool)((((int32_t)L_0) == ((int32_t)(-1)))? 1 : 0);
	}
}
// System.Int32 MiniJSON::getLastErrorIndex()
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_getLastErrorIndex_m2071674926_MetadataUsageId;
extern "C"  int32_t MiniJSON_getLastErrorIndex_m2071674926 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_getLastErrorIndex_m2071674926_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		int32_t L_0 = ((MiniJSON_t2999478751_StaticFields*)MiniJSON_t2999478751_il2cpp_TypeInfo_var->static_fields)->get_lastErrorIndex_13();
		return L_0;
	}
}
// System.String MiniJSON::getLastErrorSnippet()
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_getLastErrorSnippet_m3025045548_MetadataUsageId;
extern "C"  String_t* MiniJSON_getLastErrorSnippet_m3025045548 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_getLastErrorSnippet_m3025045548_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		int32_t L_0 = ((MiniJSON_t2999478751_StaticFields*)MiniJSON_t2999478751_il2cpp_TypeInfo_var->static_fields)->get_lastErrorIndex_13();
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_1;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		int32_t L_2 = ((MiniJSON_t2999478751_StaticFields*)MiniJSON_t2999478751_il2cpp_TypeInfo_var->static_fields)->get_lastErrorIndex_13();
		V_0 = ((int32_t)((int32_t)L_2-(int32_t)5));
		int32_t L_3 = ((MiniJSON_t2999478751_StaticFields*)MiniJSON_t2999478751_il2cpp_TypeInfo_var->static_fields)->get_lastErrorIndex_13();
		V_1 = ((int32_t)((int32_t)L_3+(int32_t)((int32_t)15)));
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		V_0 = 0;
	}

IL_002b:
	{
		int32_t L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		String_t* L_6 = ((MiniJSON_t2999478751_StaticFields*)MiniJSON_t2999478751_il2cpp_TypeInfo_var->static_fields)->get_lastDecode_14();
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m2979997331(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0048;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		String_t* L_8 = ((MiniJSON_t2999478751_StaticFields*)MiniJSON_t2999478751_il2cpp_TypeInfo_var->static_fields)->get_lastDecode_14();
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m2979997331(L_8, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		String_t* L_10 = ((MiniJSON_t2999478751_StaticFields*)MiniJSON_t2999478751_il2cpp_TypeInfo_var->static_fields)->get_lastDecode_14();
		int32_t L_11 = V_0;
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		NullCheck(L_10);
		String_t* L_14 = String_Substring_m675079568(L_10, L_11, ((int32_t)((int32_t)((int32_t)((int32_t)L_12-(int32_t)L_13))+(int32_t)1)), /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Collections.Hashtable MiniJSON::parseObject(System.Char[],System.Int32&)
extern TypeInfo* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_parseObject_m2461982797_MetadataUsageId;
extern "C"  Hashtable_t3875263730 * MiniJSON_parseObject_m2461982797 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t* ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_parseObject_m2461982797_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Hashtable_t3875263730 * V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	String_t* V_3 = NULL;
	bool V_4 = false;
	Il2CppObject * V_5 = NULL;
	{
		Hashtable_t3875263730 * L_0 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		CharU5BU5D_t3416858730* L_1 = ___json;
		int32_t* L_2 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m3797447794(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_2 = (bool)0;
		goto IL_008c;
	}

IL_0015:
	{
		CharU5BU5D_t3416858730* L_3 = ___json;
		int32_t* L_4 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		int32_t L_5 = MiniJSON_lookAhead_m2447657014(NULL /*static, unused*/, L_3, (*((int32_t*)L_4)), /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = V_1;
		if (L_6)
		{
			goto IL_0026;
		}
	}
	{
		return (Hashtable_t3875263730 *)NULL;
	}

IL_0026:
	{
		int32_t L_7 = V_1;
		if ((!(((uint32_t)L_7) == ((uint32_t)6))))
		{
			goto IL_003a;
		}
	}
	{
		CharU5BU5D_t3416858730* L_8 = ___json;
		int32_t* L_9 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m3797447794(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		goto IL_008c;
	}

IL_003a:
	{
		int32_t L_10 = V_1;
		if ((!(((uint32_t)L_10) == ((uint32_t)2))))
		{
			goto IL_004b;
		}
	}
	{
		CharU5BU5D_t3416858730* L_11 = ___json;
		int32_t* L_12 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m3797447794(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		Hashtable_t3875263730 * L_13 = V_0;
		return L_13;
	}

IL_004b:
	{
		CharU5BU5D_t3416858730* L_14 = ___json;
		int32_t* L_15 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		String_t* L_16 = MiniJSON_parseString_m561858817(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		String_t* L_17 = V_3;
		if (L_17)
		{
			goto IL_005b;
		}
	}
	{
		return (Hashtable_t3875263730 *)NULL;
	}

IL_005b:
	{
		CharU5BU5D_t3416858730* L_18 = ___json;
		int32_t* L_19 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		int32_t L_20 = MiniJSON_nextToken_m3797447794(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		int32_t L_21 = V_1;
		if ((((int32_t)L_21) == ((int32_t)5)))
		{
			goto IL_006c;
		}
	}
	{
		return (Hashtable_t3875263730 *)NULL;
	}

IL_006c:
	{
		V_4 = (bool)1;
		CharU5BU5D_t3416858730* L_22 = ___json;
		int32_t* L_23 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		Il2CppObject * L_24 = MiniJSON_parseValue_m1751092568(NULL /*static, unused*/, L_22, L_23, (&V_4), /*hidden argument*/NULL);
		V_5 = L_24;
		bool L_25 = V_4;
		if (L_25)
		{
			goto IL_0083;
		}
	}
	{
		return (Hashtable_t3875263730 *)NULL;
	}

IL_0083:
	{
		Hashtable_t3875263730 * L_26 = V_0;
		String_t* L_27 = V_3;
		Il2CppObject * L_28 = V_5;
		NullCheck(L_26);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(24 /* System.Void System.Collections.Hashtable::set_Item(System.Object,System.Object) */, L_26, L_27, L_28);
	}

IL_008c:
	{
		bool L_29 = V_2;
		if (!L_29)
		{
			goto IL_0015;
		}
	}
	{
		Hashtable_t3875263730 * L_30 = V_0;
		return L_30;
	}
}
// System.Collections.ArrayList MiniJSON::parseArray(System.Char[],System.Int32&)
extern TypeInfo* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_parseArray_m46098480_MetadataUsageId;
extern "C"  ArrayList_t2121638921 * MiniJSON_parseArray_m46098480 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t* ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_parseArray_m46098480_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ArrayList_t2121638921 * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	bool V_3 = false;
	Il2CppObject * V_4 = NULL;
	{
		ArrayList_t2121638921 * L_0 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		CharU5BU5D_t3416858730* L_1 = ___json;
		int32_t* L_2 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m3797447794(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_1 = (bool)0;
		goto IL_006c;
	}

IL_0015:
	{
		CharU5BU5D_t3416858730* L_3 = ___json;
		int32_t* L_4 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		int32_t L_5 = MiniJSON_lookAhead_m2447657014(NULL /*static, unused*/, L_3, (*((int32_t*)L_4)), /*hidden argument*/NULL);
		V_2 = L_5;
		int32_t L_6 = V_2;
		if (L_6)
		{
			goto IL_0026;
		}
	}
	{
		return (ArrayList_t2121638921 *)NULL;
	}

IL_0026:
	{
		int32_t L_7 = V_2;
		if ((!(((uint32_t)L_7) == ((uint32_t)6))))
		{
			goto IL_003a;
		}
	}
	{
		CharU5BU5D_t3416858730* L_8 = ___json;
		int32_t* L_9 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m3797447794(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		goto IL_006c;
	}

IL_003a:
	{
		int32_t L_10 = V_2;
		if ((!(((uint32_t)L_10) == ((uint32_t)4))))
		{
			goto IL_004e;
		}
	}
	{
		CharU5BU5D_t3416858730* L_11 = ___json;
		int32_t* L_12 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m3797447794(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		goto IL_0072;
	}

IL_004e:
	{
		V_3 = (bool)1;
		CharU5BU5D_t3416858730* L_13 = ___json;
		int32_t* L_14 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		Il2CppObject * L_15 = MiniJSON_parseValue_m1751092568(NULL /*static, unused*/, L_13, L_14, (&V_3), /*hidden argument*/NULL);
		V_4 = L_15;
		bool L_16 = V_3;
		if (L_16)
		{
			goto IL_0063;
		}
	}
	{
		return (ArrayList_t2121638921 *)NULL;
	}

IL_0063:
	{
		ArrayList_t2121638921 * L_17 = V_0;
		Il2CppObject * L_18 = V_4;
		NullCheck(L_17);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_17, L_18);
	}

IL_006c:
	{
		bool L_19 = V_1;
		if (!L_19)
		{
			goto IL_0015;
		}
	}

IL_0072:
	{
		ArrayList_t2121638921 * L_20 = V_0;
		return L_20;
	}
}
// System.Object MiniJSON::parseValue(System.Char[],System.Int32&,System.Boolean&)
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t534516614_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2583950;
extern Il2CppCodeGenString* _stringLiteral66658563;
extern const uint32_t MiniJSON_parseValue_m1751092568_MetadataUsageId;
extern "C"  Il2CppObject * MiniJSON_parseValue_m1751092568 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t* ___index, bool* ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_parseValue_m1751092568_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		CharU5BU5D_t3416858730* L_0 = ___json;
		int32_t* L_1 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		int32_t L_2 = MiniJSON_lookAhead_m2447657014(NULL /*static, unused*/, L_0, (*((int32_t*)L_1)), /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (L_3 == 0)
		{
			goto IL_00a3;
		}
		if (L_3 == 1)
		{
			goto IL_0059;
		}
		if (L_3 == 2)
		{
			goto IL_00a8;
		}
		if (L_3 == 3)
		{
			goto IL_0061;
		}
		if (L_3 == 4)
		{
			goto IL_00a8;
		}
		if (L_3 == 5)
		{
			goto IL_00a8;
		}
		if (L_3 == 6)
		{
			goto IL_00a8;
		}
		if (L_3 == 7)
		{
			goto IL_0044;
		}
		if (L_3 == 8)
		{
			goto IL_004c;
		}
		if (L_3 == 9)
		{
			goto IL_0069;
		}
		if (L_3 == 10)
		{
			goto IL_0081;
		}
		if (L_3 == 11)
		{
			goto IL_0099;
		}
	}
	{
		goto IL_00a8;
	}

IL_0044:
	{
		CharU5BU5D_t3416858730* L_4 = ___json;
		int32_t* L_5 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		String_t* L_6 = MiniJSON_parseString_m561858817(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_004c:
	{
		CharU5BU5D_t3416858730* L_7 = ___json;
		int32_t* L_8 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		double L_9 = MiniJSON_parseNumber_m2389640457(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		double L_10 = L_9;
		Il2CppObject * L_11 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_10);
		return L_11;
	}

IL_0059:
	{
		CharU5BU5D_t3416858730* L_12 = ___json;
		int32_t* L_13 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		Hashtable_t3875263730 * L_14 = MiniJSON_parseObject_m2461982797(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		return L_14;
	}

IL_0061:
	{
		CharU5BU5D_t3416858730* L_15 = ___json;
		int32_t* L_16 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		ArrayList_t2121638921 * L_17 = MiniJSON_parseArray_m46098480(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		return L_17;
	}

IL_0069:
	{
		CharU5BU5D_t3416858730* L_18 = ___json;
		int32_t* L_19 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m3797447794(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Boolean_t211005341_il2cpp_TypeInfo_var);
		bool L_20 = Boolean_Parse_m3007515274(NULL /*static, unused*/, _stringLiteral2583950, /*hidden argument*/NULL);
		bool L_21 = L_20;
		Il2CppObject * L_22 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_21);
		return L_22;
	}

IL_0081:
	{
		CharU5BU5D_t3416858730* L_23 = ___json;
		int32_t* L_24 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m3797447794(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Boolean_t211005341_il2cpp_TypeInfo_var);
		bool L_25 = Boolean_Parse_m3007515274(NULL /*static, unused*/, _stringLiteral66658563, /*hidden argument*/NULL);
		bool L_26 = L_25;
		Il2CppObject * L_27 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_26);
		return L_27;
	}

IL_0099:
	{
		CharU5BU5D_t3416858730* L_28 = ___json;
		int32_t* L_29 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		MiniJSON_nextToken_m3797447794(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		return NULL;
	}

IL_00a3:
	{
		goto IL_00a8;
	}

IL_00a8:
	{
		bool* L_30 = ___success;
		*((int8_t*)(L_30)) = (int8_t)0;
		return NULL;
	}
}
// System.String MiniJSON::parseString(System.Char[],System.Int32&)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern TypeInfo* Char_t2778706699_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_parseString_m561858817_MetadataUsageId;
extern "C"  String_t* MiniJSON_parseString_m561858817 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t* ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_parseString_m561858817_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	uint16_t V_1 = 0x0;
	bool V_2 = false;
	int32_t V_3 = 0;
	CharU5BU5D_t3416858730* V_4 = NULL;
	uint32_t V_5 = 0;
	int32_t V_6 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		CharU5BU5D_t3416858730* L_1 = ___json;
		int32_t* L_2 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		MiniJSON_eatWhitespace_m240573997(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		CharU5BU5D_t3416858730* L_3 = ___json;
		int32_t* L_4 = ___index;
		int32_t* L_5 = ___index;
		int32_t L_6 = (*((int32_t*)L_5));
		V_6 = L_6;
		*((int32_t*)(L_4)) = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
		int32_t L_7 = V_6;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_7);
		int32_t L_8 = L_7;
		V_1 = ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_8)));
		V_2 = (bool)0;
		goto IL_01b7;
	}

IL_0022:
	{
		int32_t* L_9 = ___index;
		CharU5BU5D_t3416858730* L_10 = ___json;
		NullCheck(L_10);
		if ((!(((uint32_t)(*((int32_t*)L_9))) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length))))))))
		{
			goto IL_0031;
		}
	}
	{
		goto IL_01bd;
	}

IL_0031:
	{
		CharU5BU5D_t3416858730* L_11 = ___json;
		int32_t* L_12 = ___index;
		int32_t* L_13 = ___index;
		int32_t L_14 = (*((int32_t*)L_13));
		V_6 = L_14;
		*((int32_t*)(L_12)) = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
		int32_t L_15 = V_6;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_15);
		int32_t L_16 = L_15;
		V_1 = ((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_16)));
		uint16_t L_17 = V_1;
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_004e;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_01bd;
	}

IL_004e:
	{
		uint16_t L_18 = V_1;
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_01aa;
		}
	}
	{
		int32_t* L_19 = ___index;
		CharU5BU5D_t3416858730* L_20 = ___json;
		NullCheck(L_20);
		if ((!(((uint32_t)(*((int32_t*)L_19))) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length))))))))
		{
			goto IL_0065;
		}
	}
	{
		goto IL_01bd;
	}

IL_0065:
	{
		CharU5BU5D_t3416858730* L_21 = ___json;
		int32_t* L_22 = ___index;
		int32_t* L_23 = ___index;
		int32_t L_24 = (*((int32_t*)L_23));
		V_6 = L_24;
		*((int32_t*)(L_22)) = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
		int32_t L_25 = V_6;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_25);
		int32_t L_26 = L_25;
		V_1 = ((L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_26)));
		uint16_t L_27 = V_1;
		if ((!(((uint32_t)L_27) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_008e;
		}
	}
	{
		String_t* L_28 = V_0;
		uint16_t L_29 = ((uint16_t)((int32_t)34));
		Il2CppObject * L_30 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_29);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = String_Concat_m389863537(NULL /*static, unused*/, L_28, L_30, /*hidden argument*/NULL);
		V_0 = L_31;
		goto IL_01a5;
	}

IL_008e:
	{
		uint16_t L_32 = V_1;
		if ((!(((uint32_t)L_32) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_00a9;
		}
	}
	{
		String_t* L_33 = V_0;
		uint16_t L_34 = ((uint16_t)((int32_t)92));
		Il2CppObject * L_35 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_34);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_36 = String_Concat_m389863537(NULL /*static, unused*/, L_33, L_35, /*hidden argument*/NULL);
		V_0 = L_36;
		goto IL_01a5;
	}

IL_00a9:
	{
		uint16_t L_37 = V_1;
		if ((!(((uint32_t)L_37) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_00c4;
		}
	}
	{
		String_t* L_38 = V_0;
		uint16_t L_39 = ((uint16_t)((int32_t)47));
		Il2CppObject * L_40 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_39);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_41 = String_Concat_m389863537(NULL /*static, unused*/, L_38, L_40, /*hidden argument*/NULL);
		V_0 = L_41;
		goto IL_01a5;
	}

IL_00c4:
	{
		uint16_t L_42 = V_1;
		if ((!(((uint32_t)L_42) == ((uint32_t)((int32_t)98)))))
		{
			goto IL_00de;
		}
	}
	{
		String_t* L_43 = V_0;
		uint16_t L_44 = ((uint16_t)8);
		Il2CppObject * L_45 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_44);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_46 = String_Concat_m389863537(NULL /*static, unused*/, L_43, L_45, /*hidden argument*/NULL);
		V_0 = L_46;
		goto IL_01a5;
	}

IL_00de:
	{
		uint16_t L_47 = V_1;
		if ((!(((uint32_t)L_47) == ((uint32_t)((int32_t)102)))))
		{
			goto IL_00f9;
		}
	}
	{
		String_t* L_48 = V_0;
		uint16_t L_49 = ((uint16_t)((int32_t)12));
		Il2CppObject * L_50 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_49);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_51 = String_Concat_m389863537(NULL /*static, unused*/, L_48, L_50, /*hidden argument*/NULL);
		V_0 = L_51;
		goto IL_01a5;
	}

IL_00f9:
	{
		uint16_t L_52 = V_1;
		if ((!(((uint32_t)L_52) == ((uint32_t)((int32_t)110)))))
		{
			goto IL_0114;
		}
	}
	{
		String_t* L_53 = V_0;
		uint16_t L_54 = ((uint16_t)((int32_t)10));
		Il2CppObject * L_55 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_54);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_56 = String_Concat_m389863537(NULL /*static, unused*/, L_53, L_55, /*hidden argument*/NULL);
		V_0 = L_56;
		goto IL_01a5;
	}

IL_0114:
	{
		uint16_t L_57 = V_1;
		if ((!(((uint32_t)L_57) == ((uint32_t)((int32_t)114)))))
		{
			goto IL_012f;
		}
	}
	{
		String_t* L_58 = V_0;
		uint16_t L_59 = ((uint16_t)((int32_t)13));
		Il2CppObject * L_60 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_59);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_61 = String_Concat_m389863537(NULL /*static, unused*/, L_58, L_60, /*hidden argument*/NULL);
		V_0 = L_61;
		goto IL_01a5;
	}

IL_012f:
	{
		uint16_t L_62 = V_1;
		if ((!(((uint32_t)L_62) == ((uint32_t)((int32_t)116)))))
		{
			goto IL_014a;
		}
	}
	{
		String_t* L_63 = V_0;
		uint16_t L_64 = ((uint16_t)((int32_t)9));
		Il2CppObject * L_65 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_64);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_66 = String_Concat_m389863537(NULL /*static, unused*/, L_63, L_65, /*hidden argument*/NULL);
		V_0 = L_66;
		goto IL_01a5;
	}

IL_014a:
	{
		uint16_t L_67 = V_1;
		if ((!(((uint32_t)L_67) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_01a5;
		}
	}
	{
		CharU5BU5D_t3416858730* L_68 = ___json;
		NullCheck(L_68);
		int32_t* L_69 = ___index;
		V_3 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_68)->max_length))))-(int32_t)(*((int32_t*)L_69))));
		int32_t L_70 = V_3;
		if ((((int32_t)L_70) < ((int32_t)4)))
		{
			goto IL_01a0;
		}
	}
	{
		V_4 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)4));
		CharU5BU5D_t3416858730* L_71 = ___json;
		int32_t* L_72 = ___index;
		CharU5BU5D_t3416858730* L_73 = V_4;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_71, (*((int32_t*)L_72)), (Il2CppArray *)(Il2CppArray *)L_73, 0, 4, /*hidden argument*/NULL);
		CharU5BU5D_t3416858730* L_74 = V_4;
		String_t* L_75 = String_CreateString_m578950865(NULL, L_74, /*hidden argument*/NULL);
		uint32_t L_76 = UInt32_Parse_m3754424175(NULL /*static, unused*/, L_75, ((int32_t)515), /*hidden argument*/NULL);
		V_5 = L_76;
		String_t* L_77 = V_0;
		uint32_t L_78 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2778706699_il2cpp_TypeInfo_var);
		String_t* L_79 = Char_ConvertFromUtf32_m567781788(NULL /*static, unused*/, L_78, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_80 = String_Concat_m138640077(NULL /*static, unused*/, L_77, L_79, /*hidden argument*/NULL);
		V_0 = L_80;
		int32_t* L_81 = ___index;
		int32_t* L_82 = ___index;
		*((int32_t*)(L_81)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_82))+(int32_t)4));
		goto IL_01a5;
	}

IL_01a0:
	{
		goto IL_01bd;
	}

IL_01a5:
	{
		goto IL_01b7;
	}

IL_01aa:
	{
		String_t* L_83 = V_0;
		uint16_t L_84 = V_1;
		uint16_t L_85 = L_84;
		Il2CppObject * L_86 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_85);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_87 = String_Concat_m389863537(NULL /*static, unused*/, L_83, L_86, /*hidden argument*/NULL);
		V_0 = L_87;
	}

IL_01b7:
	{
		bool L_88 = V_2;
		if (!L_88)
		{
			goto IL_0022;
		}
	}

IL_01bd:
	{
		bool L_89 = V_2;
		if (L_89)
		{
			goto IL_01c5;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_01c5:
	{
		String_t* L_90 = V_0;
		return L_90;
	}
}
// System.Double MiniJSON::parseNumber(System.Char[],System.Int32&)
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_parseNumber_m2389640457_MetadataUsageId;
extern "C"  double MiniJSON_parseNumber_m2389640457 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t* ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_parseNumber_m2389640457_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	CharU5BU5D_t3416858730* V_2 = NULL;
	{
		CharU5BU5D_t3416858730* L_0 = ___json;
		int32_t* L_1 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		MiniJSON_eatWhitespace_m240573997(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		CharU5BU5D_t3416858730* L_2 = ___json;
		int32_t* L_3 = ___index;
		int32_t L_4 = MiniJSON_getLastIndexOfNumber_m804658316(NULL /*static, unused*/, L_2, (*((int32_t*)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		int32_t* L_6 = ___index;
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_5-(int32_t)(*((int32_t*)L_6))))+(int32_t)1));
		int32_t L_7 = V_1;
		V_2 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)L_7));
		CharU5BU5D_t3416858730* L_8 = ___json;
		int32_t* L_9 = ___index;
		CharU5BU5D_t3416858730* L_10 = V_2;
		int32_t L_11 = V_1;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_8, (*((int32_t*)L_9)), (Il2CppArray *)(Il2CppArray *)L_10, 0, L_11, /*hidden argument*/NULL);
		int32_t* L_12 = ___index;
		int32_t L_13 = V_0;
		*((int32_t*)(L_12)) = (int32_t)((int32_t)((int32_t)L_13+(int32_t)1));
		CharU5BU5D_t3416858730* L_14 = V_2;
		String_t* L_15 = String_CreateString_m578950865(NULL, L_14, /*hidden argument*/NULL);
		double L_16 = Double_Parse_m3110783306(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Int32 MiniJSON::getLastIndexOfNumber(System.Char[],System.Int32)
extern Il2CppCodeGenString* _stringLiteral2451559847;
extern const uint32_t MiniJSON_getLastIndexOfNumber_m804658316_MetadataUsageId;
extern "C"  int32_t MiniJSON_getLastIndexOfNumber_m804658316 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_getLastIndexOfNumber_m804658316_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		goto IL_0023;
	}

IL_0007:
	{
		CharU5BU5D_t3416858730* L_1 = ___json;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		NullCheck(_stringLiteral2451559847);
		int32_t L_4 = String_IndexOf_m2775210486(_stringLiteral2451559847, ((L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3))), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_002c;
	}

IL_001f:
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_6 = V_0;
		CharU5BU5D_t3416858730* L_7 = ___json;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0007;
		}
	}

IL_002c:
	{
		int32_t L_8 = V_0;
		return ((int32_t)((int32_t)L_8-(int32_t)1));
	}
}
// System.Void MiniJSON::eatWhitespace(System.Char[],System.Int32&)
extern Il2CppCodeGenString* _stringLiteral962284;
extern const uint32_t MiniJSON_eatWhitespace_m240573997_MetadataUsageId;
extern "C"  void MiniJSON_eatWhitespace_m240573997 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t* ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_eatWhitespace_m240573997_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		goto IL_0024;
	}

IL_0005:
	{
		CharU5BU5D_t3416858730* L_0 = ___json;
		int32_t* L_1 = ___index;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, (*((int32_t*)L_1)));
		int32_t L_2 = (*((int32_t*)L_1));
		NullCheck(_stringLiteral962284);
		int32_t L_3 = String_IndexOf_m2775210486(_stringLiteral962284, ((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2))), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_001e;
		}
	}
	{
		goto IL_002e;
	}

IL_001e:
	{
		int32_t* L_4 = ___index;
		int32_t* L_5 = ___index;
		*((int32_t*)(L_4)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_5))+(int32_t)1));
	}

IL_0024:
	{
		int32_t* L_6 = ___index;
		CharU5BU5D_t3416858730* L_7 = ___json;
		NullCheck(L_7);
		if ((((int32_t)(*((int32_t*)L_6))) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0005;
		}
	}

IL_002e:
	{
		return;
	}
}
// System.Int32 MiniJSON::lookAhead(System.Char[],System.Int32)
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_lookAhead_m2447657014_MetadataUsageId;
extern "C"  int32_t MiniJSON_lookAhead_m2447657014 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_lookAhead_m2447657014_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		CharU5BU5D_t3416858730* L_1 = ___json;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		int32_t L_2 = MiniJSON_nextToken_m3797447794(NULL /*static, unused*/, L_1, (&V_0), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 MiniJSON::nextToken(System.Char[],System.Int32&)
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_nextToken_m3797447794_MetadataUsageId;
extern "C"  int32_t MiniJSON_nextToken_m3797447794 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json, int32_t* ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_nextToken_m3797447794_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0x0;
	int32_t V_1 = 0;
	uint16_t V_2 = 0x0;
	{
		CharU5BU5D_t3416858730* L_0 = ___json;
		int32_t* L_1 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		MiniJSON_eatWhitespace_m240573997(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		int32_t* L_2 = ___index;
		CharU5BU5D_t3416858730* L_3 = ___json;
		NullCheck(L_3);
		if ((!(((uint32_t)(*((int32_t*)L_2))) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))))))
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}

IL_0013:
	{
		CharU5BU5D_t3416858730* L_4 = ___json;
		int32_t* L_5 = ___index;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, (*((int32_t*)L_5)));
		int32_t L_6 = (*((int32_t*)L_5));
		V_0 = ((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
		int32_t* L_7 = ___index;
		int32_t* L_8 = ___index;
		*((int32_t*)(L_7)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_8))+(int32_t)1));
		uint16_t L_9 = V_0;
		V_2 = L_9;
		uint16_t L_10 = V_2;
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 0)
		{
			goto IL_00c6;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 1)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 2)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 3)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 4)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 5)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 6)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 7)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 8)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 9)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 10)
		{
			goto IL_00c4;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 11)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 12)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 13)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 14)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 15)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 16)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 17)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 18)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 19)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 20)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 21)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 22)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 23)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 24)
		{
			goto IL_00ca;
		}
	}

IL_008d:
	{
		uint16_t L_11 = V_2;
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)91))) == 0)
		{
			goto IL_00c0;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)91))) == 1)
		{
			goto IL_00a2;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)91))) == 2)
		{
			goto IL_00c2;
		}
	}

IL_00a2:
	{
		uint16_t L_12 = V_2;
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)123))) == 0)
		{
			goto IL_00bc;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)123))) == 1)
		{
			goto IL_00cc;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)123))) == 2)
		{
			goto IL_00be;
		}
	}
	{
		goto IL_00cc;
	}

IL_00bc:
	{
		return 1;
	}

IL_00be:
	{
		return 2;
	}

IL_00c0:
	{
		return 3;
	}

IL_00c2:
	{
		return 4;
	}

IL_00c4:
	{
		return 6;
	}

IL_00c6:
	{
		return 7;
	}

IL_00c8:
	{
		return 8;
	}

IL_00ca:
	{
		return 5;
	}

IL_00cc:
	{
		int32_t* L_13 = ___index;
		int32_t* L_14 = ___index;
		*((int32_t*)(L_13)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_14))-(int32_t)1));
		CharU5BU5D_t3416858730* L_15 = ___json;
		NullCheck(L_15);
		int32_t* L_16 = ___index;
		V_1 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length))))-(int32_t)(*((int32_t*)L_16))));
		int32_t L_17 = V_1;
		if ((((int32_t)L_17) < ((int32_t)5)))
		{
			goto IL_0128;
		}
	}
	{
		CharU5BU5D_t3416858730* L_18 = ___json;
		int32_t* L_19 = ___index;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, (*((int32_t*)L_19)));
		int32_t L_20 = (*((int32_t*)L_19));
		if ((!(((uint32_t)((L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20)))) == ((uint32_t)((int32_t)102)))))
		{
			goto IL_0128;
		}
	}
	{
		CharU5BU5D_t3416858730* L_21 = ___json;
		int32_t* L_22 = ___index;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, ((int32_t)((int32_t)(*((int32_t*)L_22))+(int32_t)1)));
		int32_t L_23 = ((int32_t)((int32_t)(*((int32_t*)L_22))+(int32_t)1));
		if ((!(((uint32_t)((L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23)))) == ((uint32_t)((int32_t)97)))))
		{
			goto IL_0128;
		}
	}
	{
		CharU5BU5D_t3416858730* L_24 = ___json;
		int32_t* L_25 = ___index;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, ((int32_t)((int32_t)(*((int32_t*)L_25))+(int32_t)2)));
		int32_t L_26 = ((int32_t)((int32_t)(*((int32_t*)L_25))+(int32_t)2));
		if ((!(((uint32_t)((L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26)))) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_0128;
		}
	}
	{
		CharU5BU5D_t3416858730* L_27 = ___json;
		int32_t* L_28 = ___index;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, ((int32_t)((int32_t)(*((int32_t*)L_28))+(int32_t)3)));
		int32_t L_29 = ((int32_t)((int32_t)(*((int32_t*)L_28))+(int32_t)3));
		if ((!(((uint32_t)((L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29)))) == ((uint32_t)((int32_t)115)))))
		{
			goto IL_0128;
		}
	}
	{
		CharU5BU5D_t3416858730* L_30 = ___json;
		int32_t* L_31 = ___index;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, ((int32_t)((int32_t)(*((int32_t*)L_31))+(int32_t)4)));
		int32_t L_32 = ((int32_t)((int32_t)(*((int32_t*)L_31))+(int32_t)4));
		if ((!(((uint32_t)((L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32)))) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_0128;
		}
	}
	{
		int32_t* L_33 = ___index;
		int32_t* L_34 = ___index;
		*((int32_t*)(L_33)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_34))+(int32_t)5));
		return ((int32_t)10);
	}

IL_0128:
	{
		int32_t L_35 = V_1;
		if ((((int32_t)L_35) < ((int32_t)4)))
		{
			goto IL_016a;
		}
	}
	{
		CharU5BU5D_t3416858730* L_36 = ___json;
		int32_t* L_37 = ___index;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, (*((int32_t*)L_37)));
		int32_t L_38 = (*((int32_t*)L_37));
		if ((!(((uint32_t)((L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_38)))) == ((uint32_t)((int32_t)116)))))
		{
			goto IL_016a;
		}
	}
	{
		CharU5BU5D_t3416858730* L_39 = ___json;
		int32_t* L_40 = ___index;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, ((int32_t)((int32_t)(*((int32_t*)L_40))+(int32_t)1)));
		int32_t L_41 = ((int32_t)((int32_t)(*((int32_t*)L_40))+(int32_t)1));
		if ((!(((uint32_t)((L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_41)))) == ((uint32_t)((int32_t)114)))))
		{
			goto IL_016a;
		}
	}
	{
		CharU5BU5D_t3416858730* L_42 = ___json;
		int32_t* L_43 = ___index;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, ((int32_t)((int32_t)(*((int32_t*)L_43))+(int32_t)2)));
		int32_t L_44 = ((int32_t)((int32_t)(*((int32_t*)L_43))+(int32_t)2));
		if ((!(((uint32_t)((L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_44)))) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_016a;
		}
	}
	{
		CharU5BU5D_t3416858730* L_45 = ___json;
		int32_t* L_46 = ___index;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, ((int32_t)((int32_t)(*((int32_t*)L_46))+(int32_t)3)));
		int32_t L_47 = ((int32_t)((int32_t)(*((int32_t*)L_46))+(int32_t)3));
		if ((!(((uint32_t)((L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47)))) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_016a;
		}
	}
	{
		int32_t* L_48 = ___index;
		int32_t* L_49 = ___index;
		*((int32_t*)(L_48)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_49))+(int32_t)4));
		return ((int32_t)9);
	}

IL_016a:
	{
		int32_t L_50 = V_1;
		if ((((int32_t)L_50) < ((int32_t)4)))
		{
			goto IL_01ac;
		}
	}
	{
		CharU5BU5D_t3416858730* L_51 = ___json;
		int32_t* L_52 = ___index;
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, (*((int32_t*)L_52)));
		int32_t L_53 = (*((int32_t*)L_52));
		if ((!(((uint32_t)((L_51)->GetAt(static_cast<il2cpp_array_size_t>(L_53)))) == ((uint32_t)((int32_t)110)))))
		{
			goto IL_01ac;
		}
	}
	{
		CharU5BU5D_t3416858730* L_54 = ___json;
		int32_t* L_55 = ___index;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)((int32_t)(*((int32_t*)L_55))+(int32_t)1)));
		int32_t L_56 = ((int32_t)((int32_t)(*((int32_t*)L_55))+(int32_t)1));
		if ((!(((uint32_t)((L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_56)))) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_01ac;
		}
	}
	{
		CharU5BU5D_t3416858730* L_57 = ___json;
		int32_t* L_58 = ___index;
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, ((int32_t)((int32_t)(*((int32_t*)L_58))+(int32_t)2)));
		int32_t L_59 = ((int32_t)((int32_t)(*((int32_t*)L_58))+(int32_t)2));
		if ((!(((uint32_t)((L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_59)))) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_01ac;
		}
	}
	{
		CharU5BU5D_t3416858730* L_60 = ___json;
		int32_t* L_61 = ___index;
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, ((int32_t)((int32_t)(*((int32_t*)L_61))+(int32_t)3)));
		int32_t L_62 = ((int32_t)((int32_t)(*((int32_t*)L_61))+(int32_t)3));
		if ((!(((uint32_t)((L_60)->GetAt(static_cast<il2cpp_array_size_t>(L_62)))) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_01ac;
		}
	}
	{
		int32_t* L_63 = ___index;
		int32_t* L_64 = ___index;
		*((int32_t*)(L_63)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_64))+(int32_t)4));
		return ((int32_t)11);
	}

IL_01ac:
	{
		return 0;
	}
}
// System.Boolean MiniJSON::serializeObjectOrArray(System.Object,System.Text.StringBuilder)
extern TypeInfo* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern TypeInfo* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_serializeObjectOrArray_m1421528817_MetadataUsageId;
extern "C"  bool MiniJSON_serializeObjectOrArray_m1421528817 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___objectOrArray, StringBuilder_t3822575854 * ___builder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_serializeObjectOrArray_m1421528817_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___objectOrArray;
		if (!((Hashtable_t3875263730 *)IsInstClass(L_0, Hashtable_t3875263730_il2cpp_TypeInfo_var)))
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject * L_1 = ___objectOrArray;
		StringBuilder_t3822575854 * L_2 = ___builder;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		bool L_3 = MiniJSON_serializeObject_m2476982477(NULL /*static, unused*/, ((Hashtable_t3875263730 *)CastclassClass(L_1, Hashtable_t3875263730_il2cpp_TypeInfo_var)), L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0018:
	{
		Il2CppObject * L_4 = ___objectOrArray;
		if (!((ArrayList_t2121638921 *)IsInstClass(L_4, ArrayList_t2121638921_il2cpp_TypeInfo_var)))
		{
			goto IL_0030;
		}
	}
	{
		Il2CppObject * L_5 = ___objectOrArray;
		StringBuilder_t3822575854 * L_6 = ___builder;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		bool L_7 = MiniJSON_serializeArray_m1996381038(NULL /*static, unused*/, ((ArrayList_t2121638921 *)CastclassClass(L_5, ArrayList_t2121638921_il2cpp_TypeInfo_var)), L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0030:
	{
		return (bool)0;
	}
}
// System.Boolean MiniJSON::serializeObject(System.Collections.Hashtable,System.Text.StringBuilder)
extern TypeInfo* IDictionaryEnumerator_t1541724277_il2cpp_TypeInfo_var;
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral58;
extern Il2CppCodeGenString* _stringLiteral125;
extern const uint32_t MiniJSON_serializeObject_m2476982477_MetadataUsageId;
extern "C"  bool MiniJSON_serializeObject_m2476982477 (Il2CppObject * __this /* static, unused */, Hashtable_t3875263730 * ___anObject, StringBuilder_t3822575854 * ___builder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_serializeObject_m2476982477_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	String_t* V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	{
		StringBuilder_t3822575854 * L_0 = ___builder;
		NullCheck(L_0);
		StringBuilder_Append_m3898090075(L_0, _stringLiteral123, /*hidden argument*/NULL);
		Hashtable_t3875263730 * L_1 = ___anObject;
		NullCheck(L_1);
		Il2CppObject * L_2 = VirtFuncInvoker0< Il2CppObject * >::Invoke(29 /* System.Collections.IDictionaryEnumerator System.Collections.Hashtable::GetEnumerator() */, L_1);
		V_0 = L_2;
		V_1 = (bool)1;
		goto IL_0062;
	}

IL_001a:
	{
		Il2CppObject * L_3 = V_0;
		NullCheck(L_3);
		Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(1 /* System.Object System.Collections.IDictionaryEnumerator::get_Key() */, IDictionaryEnumerator_t1541724277_il2cpp_TypeInfo_var, L_3);
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		V_2 = L_5;
		Il2CppObject * L_6 = V_0;
		NullCheck(L_6);
		Il2CppObject * L_7 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionaryEnumerator::get_Value() */, IDictionaryEnumerator_t1541724277_il2cpp_TypeInfo_var, L_6);
		V_3 = L_7;
		bool L_8 = V_1;
		if (L_8)
		{
			goto IL_003f;
		}
	}
	{
		StringBuilder_t3822575854 * L_9 = ___builder;
		NullCheck(L_9);
		StringBuilder_Append_m3898090075(L_9, _stringLiteral1396, /*hidden argument*/NULL);
	}

IL_003f:
	{
		String_t* L_10 = V_2;
		StringBuilder_t3822575854 * L_11 = ___builder;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		MiniJSON_serializeString_m2203421607(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_12 = ___builder;
		NullCheck(L_12);
		StringBuilder_Append_m3898090075(L_12, _stringLiteral58, /*hidden argument*/NULL);
		Il2CppObject * L_13 = V_3;
		StringBuilder_t3822575854 * L_14 = ___builder;
		bool L_15 = MiniJSON_serializeValue_m1454356919(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0060;
		}
	}
	{
		return (bool)0;
	}

IL_0060:
	{
		V_1 = (bool)0;
	}

IL_0062:
	{
		Il2CppObject * L_16 = V_0;
		NullCheck(L_16);
		bool L_17 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_16);
		if (L_17)
		{
			goto IL_001a;
		}
	}
	{
		StringBuilder_t3822575854 * L_18 = ___builder;
		NullCheck(L_18);
		StringBuilder_Append_m3898090075(L_18, _stringLiteral125, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Boolean MiniJSON::serializeDictionary(System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Text.StringBuilder)
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t2373214747_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m3984337014_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4096864128_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m2387692660_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m899304327_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2276346559_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral58;
extern Il2CppCodeGenString* _stringLiteral125;
extern const uint32_t MiniJSON_serializeDictionary_m2089695709_MetadataUsageId;
extern "C"  bool MiniJSON_serializeDictionary_m2089695709 (Il2CppObject * __this /* static, unused */, Dictionary_2_t2606186806 * ___dict, StringBuilder_t3822575854 * ___builder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_serializeDictionary_m2089695709_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	KeyValuePair_2_t2094718104  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t2373214747  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t3822575854 * L_0 = ___builder;
		NullCheck(L_0);
		StringBuilder_Append_m3898090075(L_0, _stringLiteral123, /*hidden argument*/NULL);
		V_0 = (bool)1;
		Dictionary_2_t2606186806 * L_1 = ___dict;
		NullCheck(L_1);
		Enumerator_t2373214747  L_2 = Dictionary_2_GetEnumerator_m3984337014(L_1, /*hidden argument*/Dictionary_2_GetEnumerator_m3984337014_MethodInfo_var);
		V_2 = L_2;
	}

IL_0015:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005c;
		}

IL_001a:
		{
			KeyValuePair_2_t2094718104  L_3 = Enumerator_get_Current_m4096864128((&V_2), /*hidden argument*/Enumerator_get_Current_m4096864128_MethodInfo_var);
			V_1 = L_3;
			bool L_4 = V_0;
			if (L_4)
			{
				goto IL_0034;
			}
		}

IL_0028:
		{
			StringBuilder_t3822575854 * L_5 = ___builder;
			NullCheck(L_5);
			StringBuilder_Append_m3898090075(L_5, _stringLiteral1396, /*hidden argument*/NULL);
		}

IL_0034:
		{
			String_t* L_6 = KeyValuePair_2_get_Key_m2387692660((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m2387692660_MethodInfo_var);
			StringBuilder_t3822575854 * L_7 = ___builder;
			IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
			MiniJSON_serializeString_m2203421607(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
			StringBuilder_t3822575854 * L_8 = ___builder;
			NullCheck(L_8);
			StringBuilder_Append_m3898090075(L_8, _stringLiteral58, /*hidden argument*/NULL);
			String_t* L_9 = KeyValuePair_2_get_Value_m899304327((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m899304327_MethodInfo_var);
			StringBuilder_t3822575854 * L_10 = ___builder;
			MiniJSON_serializeString_m2203421607(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
			V_0 = (bool)0;
		}

IL_005c:
		{
			bool L_11 = Enumerator_MoveNext_m2276346559((&V_2), /*hidden argument*/Enumerator_MoveNext_m2276346559_MethodInfo_var);
			if (L_11)
			{
				goto IL_001a;
			}
		}

IL_0068:
		{
			IL2CPP_LEAVE(0x79, FINALLY_006d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_006d;
	}

FINALLY_006d:
	{ // begin finally (depth: 1)
		Enumerator_t2373214747  L_12 = V_2;
		Enumerator_t2373214747  L_13 = L_12;
		Il2CppObject * L_14 = Box(Enumerator_t2373214747_il2cpp_TypeInfo_var, &L_13);
		NullCheck((Il2CppObject *)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
		IL2CPP_END_FINALLY(109)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(109)
	{
		IL2CPP_JUMP_TBL(0x79, IL_0079)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0079:
	{
		StringBuilder_t3822575854 * L_15 = ___builder;
		NullCheck(L_15);
		StringBuilder_Append_m3898090075(L_15, _stringLiteral125, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Boolean MiniJSON::serializeArray(System.Collections.ArrayList,System.Text.StringBuilder)
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t MiniJSON_serializeArray_m1996381038_MetadataUsageId;
extern "C"  bool MiniJSON_serializeArray_m1996381038 (Il2CppObject * __this /* static, unused */, ArrayList_t2121638921 * ___anArray, StringBuilder_t3822575854 * ___builder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_serializeArray_m1996381038_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	{
		StringBuilder_t3822575854 * L_0 = ___builder;
		NullCheck(L_0);
		StringBuilder_Append_m3898090075(L_0, _stringLiteral91, /*hidden argument*/NULL);
		V_0 = (bool)1;
		V_1 = 0;
		goto IL_0043;
	}

IL_0015:
	{
		ArrayList_t2121638921 * L_1 = ___anArray;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		Il2CppObject * L_3 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_1, L_2);
		V_2 = L_3;
		bool L_4 = V_0;
		if (L_4)
		{
			goto IL_002f;
		}
	}
	{
		StringBuilder_t3822575854 * L_5 = ___builder;
		NullCheck(L_5);
		StringBuilder_Append_m3898090075(L_5, _stringLiteral1396, /*hidden argument*/NULL);
	}

IL_002f:
	{
		Il2CppObject * L_6 = V_2;
		StringBuilder_t3822575854 * L_7 = ___builder;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		bool L_8 = MiniJSON_serializeValue_m1454356919(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_003d;
		}
	}
	{
		return (bool)0;
	}

IL_003d:
	{
		V_0 = (bool)0;
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0043:
	{
		int32_t L_10 = V_1;
		ArrayList_t2121638921 * L_11 = ___anArray;
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_11);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0015;
		}
	}
	{
		StringBuilder_t3822575854 * L_13 = ___builder;
		NullCheck(L_13);
		StringBuilder_Append_m3898090075(L_13, _stringLiteral93, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Boolean MiniJSON::serializeValue(System.Object,System.Text.StringBuilder)
extern TypeInfo* ICollection_t3761522009_il2cpp_TypeInfo_var;
extern TypeInfo* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Char_t2778706699_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1097883944_il2cpp_TypeInfo_var;
extern TypeInfo* Decimal_t1688557254_il2cpp_TypeInfo_var;
extern TypeInfo* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t2606186806_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3392903;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral97196323;
extern const uint32_t MiniJSON_serializeValue_m1454356919_MetadataUsageId;
extern "C"  bool MiniJSON_serializeValue_m1454356919 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value, StringBuilder_t3822575854 * ___builder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_serializeValue_m1454356919_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___value;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		StringBuilder_t3822575854 * L_1 = ___builder;
		NullCheck(L_1);
		StringBuilder_Append_m3898090075(L_1, _stringLiteral3392903, /*hidden argument*/NULL);
		goto IL_0164;
	}

IL_0017:
	{
		Il2CppObject * L_2 = ___value;
		NullCheck(L_2);
		Type_t * L_3 = Object_GetType_m2022236990(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(21 /* System.Boolean System.Type::get_IsArray() */, L_3);
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		Il2CppObject * L_5 = ___value;
		ArrayList_t2121638921 * L_6 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m2944534244(L_6, ((Il2CppObject *)Castclass(L_5, ICollection_t3761522009_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_7 = ___builder;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		MiniJSON_serializeArray_m1996381038(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		goto IL_0164;
	}

IL_003e:
	{
		Il2CppObject * L_8 = ___value;
		if (!((String_t*)IsInstSealed(L_8, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_005a;
		}
	}
	{
		Il2CppObject * L_9 = ___value;
		StringBuilder_t3822575854 * L_10 = ___builder;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		MiniJSON_serializeString_m2203421607(NULL /*static, unused*/, ((String_t*)CastclassSealed(L_9, String_t_il2cpp_TypeInfo_var)), L_10, /*hidden argument*/NULL);
		goto IL_0164;
	}

IL_005a:
	{
		Il2CppObject * L_11 = ___value;
		if (!((Il2CppObject *)IsInstSealed(L_11, Char_t2778706699_il2cpp_TypeInfo_var)))
		{
			goto IL_007b;
		}
	}
	{
		Il2CppObject * L_12 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		String_t* L_13 = Convert_ToString_m3462155176(NULL /*static, unused*/, ((*(uint16_t*)((uint16_t*)UnBox (L_12, Char_t2778706699_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_14 = ___builder;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		MiniJSON_serializeString_m2203421607(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		goto IL_0164;
	}

IL_007b:
	{
		Il2CppObject * L_15 = ___value;
		if (!((Il2CppObject *)IsInstSealed(L_15, Decimal_t1688557254_il2cpp_TypeInfo_var)))
		{
			goto IL_009c;
		}
	}
	{
		Il2CppObject * L_16 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		String_t* L_17 = Convert_ToString_m851570371(NULL /*static, unused*/, ((*(Decimal_t1688557254 *)((Decimal_t1688557254 *)UnBox (L_16, Decimal_t1688557254_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_18 = ___builder;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		MiniJSON_serializeString_m2203421607(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		goto IL_0164;
	}

IL_009c:
	{
		Il2CppObject * L_19 = ___value;
		if (!((Hashtable_t3875263730 *)IsInstClass(L_19, Hashtable_t3875263730_il2cpp_TypeInfo_var)))
		{
			goto IL_00b9;
		}
	}
	{
		Il2CppObject * L_20 = ___value;
		StringBuilder_t3822575854 * L_21 = ___builder;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		MiniJSON_serializeObject_m2476982477(NULL /*static, unused*/, ((Hashtable_t3875263730 *)CastclassClass(L_20, Hashtable_t3875263730_il2cpp_TypeInfo_var)), L_21, /*hidden argument*/NULL);
		goto IL_0164;
	}

IL_00b9:
	{
		Il2CppObject * L_22 = ___value;
		if (!((Dictionary_2_t2606186806 *)IsInstClass(L_22, Dictionary_2_t2606186806_il2cpp_TypeInfo_var)))
		{
			goto IL_00d6;
		}
	}
	{
		Il2CppObject * L_23 = ___value;
		StringBuilder_t3822575854 * L_24 = ___builder;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		MiniJSON_serializeDictionary_m2089695709(NULL /*static, unused*/, ((Dictionary_2_t2606186806 *)CastclassClass(L_23, Dictionary_2_t2606186806_il2cpp_TypeInfo_var)), L_24, /*hidden argument*/NULL);
		goto IL_0164;
	}

IL_00d6:
	{
		Il2CppObject * L_25 = ___value;
		if (!((ArrayList_t2121638921 *)IsInstClass(L_25, ArrayList_t2121638921_il2cpp_TypeInfo_var)))
		{
			goto IL_00f3;
		}
	}
	{
		Il2CppObject * L_26 = ___value;
		StringBuilder_t3822575854 * L_27 = ___builder;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		MiniJSON_serializeArray_m1996381038(NULL /*static, unused*/, ((ArrayList_t2121638921 *)CastclassClass(L_26, ArrayList_t2121638921_il2cpp_TypeInfo_var)), L_27, /*hidden argument*/NULL);
		goto IL_0164;
	}

IL_00f3:
	{
		Il2CppObject * L_28 = ___value;
		if (!((Il2CppObject *)IsInstSealed(L_28, Boolean_t211005341_il2cpp_TypeInfo_var)))
		{
			goto IL_011a;
		}
	}
	{
		Il2CppObject * L_29 = ___value;
		if (!((*(bool*)((bool*)UnBox (L_29, Boolean_t211005341_il2cpp_TypeInfo_var)))))
		{
			goto IL_011a;
		}
	}
	{
		StringBuilder_t3822575854 * L_30 = ___builder;
		NullCheck(L_30);
		StringBuilder_Append_m3898090075(L_30, _stringLiteral3569038, /*hidden argument*/NULL);
		goto IL_0164;
	}

IL_011a:
	{
		Il2CppObject * L_31 = ___value;
		if (!((Il2CppObject *)IsInstSealed(L_31, Boolean_t211005341_il2cpp_TypeInfo_var)))
		{
			goto IL_0141;
		}
	}
	{
		Il2CppObject * L_32 = ___value;
		if (((*(bool*)((bool*)UnBox (L_32, Boolean_t211005341_il2cpp_TypeInfo_var)))))
		{
			goto IL_0141;
		}
	}
	{
		StringBuilder_t3822575854 * L_33 = ___builder;
		NullCheck(L_33);
		StringBuilder_Append_m3898090075(L_33, _stringLiteral97196323, /*hidden argument*/NULL);
		goto IL_0164;
	}

IL_0141:
	{
		Il2CppObject * L_34 = ___value;
		NullCheck(L_34);
		Type_t * L_35 = Object_GetType_m2022236990(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		bool L_36 = VirtFuncInvoker0< bool >::Invoke(30 /* System.Boolean System.Type::get_IsPrimitive() */, L_35);
		if (!L_36)
		{
			goto IL_0162;
		}
	}
	{
		Il2CppObject * L_37 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		double L_38 = Convert_ToDouble_m1941295007(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_39 = ___builder;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		MiniJSON_serializeNumber_m3607534511(NULL /*static, unused*/, L_38, L_39, /*hidden argument*/NULL);
		goto IL_0164;
	}

IL_0162:
	{
		return (bool)0;
	}

IL_0164:
	{
		return (bool)1;
	}
}
// System.Void MiniJSON::serializeString(System.String,System.Text.StringBuilder)
extern TypeInfo* Convert_t1097883944_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral34;
extern Il2CppCodeGenString* _stringLiteral2886;
extern Il2CppCodeGenString* _stringLiteral2944;
extern Il2CppCodeGenString* _stringLiteral2950;
extern Il2CppCodeGenString* _stringLiteral2954;
extern Il2CppCodeGenString* _stringLiteral2962;
extern Il2CppCodeGenString* _stringLiteral2966;
extern Il2CppCodeGenString* _stringLiteral2968;
extern Il2CppCodeGenString* _stringLiteral2969;
extern const uint32_t MiniJSON_serializeString_m2203421607_MetadataUsageId;
extern "C"  void MiniJSON_serializeString_m2203421607 (Il2CppObject * __this /* static, unused */, String_t* ___aString, StringBuilder_t3822575854 * ___builder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_serializeString_m2203421607_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CharU5BU5D_t3416858730* V_0 = NULL;
	int32_t V_1 = 0;
	uint16_t V_2 = 0x0;
	int32_t V_3 = 0;
	{
		StringBuilder_t3822575854 * L_0 = ___builder;
		NullCheck(L_0);
		StringBuilder_Append_m3898090075(L_0, _stringLiteral34, /*hidden argument*/NULL);
		String_t* L_1 = ___aString;
		NullCheck(L_1);
		CharU5BU5D_t3416858730* L_2 = String_ToCharArray_m1208288742(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = 0;
		goto IL_0115;
	}

IL_001a:
	{
		CharU5BU5D_t3416858730* L_3 = V_0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_2 = ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
		uint16_t L_6 = V_2;
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0037;
		}
	}
	{
		StringBuilder_t3822575854 * L_7 = ___builder;
		NullCheck(L_7);
		StringBuilder_Append_m3898090075(L_7, _stringLiteral2886, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_0037:
	{
		uint16_t L_8 = V_2;
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_0050;
		}
	}
	{
		StringBuilder_t3822575854 * L_9 = ___builder;
		NullCheck(L_9);
		StringBuilder_Append_m3898090075(L_9, _stringLiteral2944, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_0050:
	{
		uint16_t L_10 = V_2;
		if ((!(((uint32_t)L_10) == ((uint32_t)8))))
		{
			goto IL_0068;
		}
	}
	{
		StringBuilder_t3822575854 * L_11 = ___builder;
		NullCheck(L_11);
		StringBuilder_Append_m3898090075(L_11, _stringLiteral2950, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_0068:
	{
		uint16_t L_12 = V_2;
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_0081;
		}
	}
	{
		StringBuilder_t3822575854 * L_13 = ___builder;
		NullCheck(L_13);
		StringBuilder_Append_m3898090075(L_13, _stringLiteral2954, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_0081:
	{
		uint16_t L_14 = V_2;
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_009a;
		}
	}
	{
		StringBuilder_t3822575854 * L_15 = ___builder;
		NullCheck(L_15);
		StringBuilder_Append_m3898090075(L_15, _stringLiteral2962, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_009a:
	{
		uint16_t L_16 = V_2;
		if ((!(((uint32_t)L_16) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_00b3;
		}
	}
	{
		StringBuilder_t3822575854 * L_17 = ___builder;
		NullCheck(L_17);
		StringBuilder_Append_m3898090075(L_17, _stringLiteral2966, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_00b3:
	{
		uint16_t L_18 = V_2;
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_00cc;
		}
	}
	{
		StringBuilder_t3822575854 * L_19 = ___builder;
		NullCheck(L_19);
		StringBuilder_Append_m3898090075(L_19, _stringLiteral2968, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_00cc:
	{
		uint16_t L_20 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		int32_t L_21 = Convert_ToInt32_m100832938(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		V_3 = L_21;
		int32_t L_22 = V_3;
		if ((((int32_t)L_22) < ((int32_t)((int32_t)32))))
		{
			goto IL_00f0;
		}
	}
	{
		int32_t L_23 = V_3;
		if ((((int32_t)L_23) > ((int32_t)((int32_t)126))))
		{
			goto IL_00f0;
		}
	}
	{
		StringBuilder_t3822575854 * L_24 = ___builder;
		uint16_t L_25 = V_2;
		NullCheck(L_24);
		StringBuilder_Append_m2143093878(L_24, L_25, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_00f0:
	{
		StringBuilder_t3822575854 * L_26 = ___builder;
		int32_t L_27 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		String_t* L_28 = Convert_ToString_m3908657329(NULL /*static, unused*/, L_27, ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_28);
		String_t* L_29 = String_PadLeft_m3268206439(L_28, 4, ((int32_t)48), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2969, L_29, /*hidden argument*/NULL);
		NullCheck(L_26);
		StringBuilder_Append_m3898090075(L_26, L_30, /*hidden argument*/NULL);
	}

IL_0111:
	{
		int32_t L_31 = V_1;
		V_1 = ((int32_t)((int32_t)L_31+(int32_t)1));
	}

IL_0115:
	{
		int32_t L_32 = V_1;
		CharU5BU5D_t3416858730* L_33 = V_0;
		NullCheck(L_33);
		if ((((int32_t)L_32) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_33)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		StringBuilder_t3822575854 * L_34 = ___builder;
		NullCheck(L_34);
		StringBuilder_Append_m3898090075(L_34, _stringLiteral34, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MiniJSON::serializeNumber(System.Double,System.Text.StringBuilder)
extern TypeInfo* Convert_t1097883944_il2cpp_TypeInfo_var;
extern const uint32_t MiniJSON_serializeNumber_m3607534511_MetadataUsageId;
extern "C"  void MiniJSON_serializeNumber_m3607534511 (Il2CppObject * __this /* static, unused */, double ___number, StringBuilder_t3822575854 * ___builder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJSON_serializeNumber_m3607534511_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringBuilder_t3822575854 * L_0 = ___builder;
		double L_1 = ___number;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		String_t* L_2 = Convert_ToString_m3932406093(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		StringBuilder_Append_m3898090075(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.String MiniJsonExtensions::toJson(System.Collections.Hashtable)
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern const uint32_t MiniJsonExtensions_toJson_m1205538118_MetadataUsageId;
extern "C"  String_t* MiniJsonExtensions_toJson_m1205538118 (Il2CppObject * __this /* static, unused */, Hashtable_t3875263730 * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJsonExtensions_toJson_m1205538118_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t3875263730 * L_0 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		String_t* L_1 = MiniJSON_jsonEncode_m3503903903(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String MiniJsonExtensions::toJson(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern const uint32_t MiniJsonExtensions_toJson_m2635768255_MetadataUsageId;
extern "C"  String_t* MiniJsonExtensions_toJson_m2635768255 (Il2CppObject * __this /* static, unused */, Dictionary_2_t2606186806 * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJsonExtensions_toJson_m2635768255_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2606186806 * L_0 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		String_t* L_1 = MiniJSON_jsonEncode_m3503903903(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.ArrayList MiniJsonExtensions::arrayListFromJson(System.String)
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern TypeInfo* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern const uint32_t MiniJsonExtensions_arrayListFromJson_m2234629349_MetadataUsageId;
extern "C"  ArrayList_t2121638921 * MiniJsonExtensions_arrayListFromJson_m2234629349 (Il2CppObject * __this /* static, unused */, String_t* ___json, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJsonExtensions_arrayListFromJson_m2234629349_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___json;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = MiniJSON_jsonDecode_m2239788899(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return ((ArrayList_t2121638921 *)IsInstClass(L_1, ArrayList_t2121638921_il2cpp_TypeInfo_var));
	}
}
// System.Collections.Hashtable MiniJsonExtensions::hashtableFromJson(System.String)
extern TypeInfo* MiniJSON_t2999478751_il2cpp_TypeInfo_var;
extern TypeInfo* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern const uint32_t MiniJsonExtensions_hashtableFromJson_m1478403219_MetadataUsageId;
extern "C"  Hashtable_t3875263730 * MiniJsonExtensions_hashtableFromJson_m1478403219 (Il2CppObject * __this /* static, unused */, String_t* ___json, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MiniJsonExtensions_hashtableFromJson_m1478403219_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___json;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJSON_t2999478751_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = MiniJSON_jsonDecode_m2239788899(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return ((Hashtable_t3875263730 *)IsInstClass(L_1, Hashtable_t3875263730_il2cpp_TypeInfo_var));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
