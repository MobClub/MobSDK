﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// com.mob.ShareREC
struct ShareREC_t3659413906;
// System.String
struct String_t;
// com.mob.FinishedRecordEvent
struct FinishedRecordEvent_t4162477592;
// System.Collections.Hashtable
struct Hashtable_t3875263730;
// com.mob.CloseEvent
struct CloseEvent_t4246496483;
// com.mob.EditResultEvent
struct EditResultEvent_t127194420;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_FinishedReco4162477592.h"
#include "mscorlib_System_Collections_Hashtable3875263730.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_CloseEvent4246496483.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_EditResultEve127194420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_SocialPageTyp127466071.h"

// System.Void com.mob.ShareREC::.ctor()
extern "C"  void ShareREC__ctor_m2604471299 (ShareREC_t3659413906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareREC::shareRECCallback(System.String)
extern "C"  void ShareREC_shareRECCallback_m2793148811 (ShareREC_t3659413906 * __this, String_t* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareREC::setCallbackObjectName(System.String)
extern "C"  void ShareREC_setCallbackObjectName_m1196151472 (Il2CppObject * __this /* static, unused */, String_t* ___objectName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareREC::registerApp(System.String)
extern "C"  void ShareREC_registerApp_m104679203 (Il2CppObject * __this /* static, unused */, String_t* ___appKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareREC::startRecoring()
extern "C"  void ShareREC_startRecoring_m2705032210 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareREC::stopRecording(com.mob.FinishedRecordEvent)
extern "C"  void ShareREC_stopRecording_m2331859558 (Il2CppObject * __this /* static, unused */, FinishedRecordEvent_t4162477592 * ___evt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareREC::playLastRecording()
extern "C"  void ShareREC_playLastRecording_m2258034856 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareREC::setBitRate(System.Int32)
extern "C"  void ShareREC_setBitRate_m3609732637 (Il2CppObject * __this /* static, unused */, int32_t ___bitRate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareREC::setFPS(System.Int32)
extern "C"  void ShareREC_setFPS_m4062387577 (Il2CppObject * __this /* static, unused */, int32_t ___fps, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareREC::setMinimumRecordingTime(System.Single)
extern "C"  void ShareREC_setMinimumRecordingTime_m2717129048 (Il2CppObject * __this /* static, unused */, float ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String com.mob.ShareREC::lastRecordingPath()
extern "C"  String_t* ShareREC_lastRecordingPath_m2454543038 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareREC::editLastingRecording(System.String,System.Collections.Hashtable,com.mob.CloseEvent)
extern "C"  void ShareREC_editLastingRecording_m3284923037 (Il2CppObject * __this /* static, unused */, String_t* ___title, Hashtable_t3875263730 * ___userData, CloseEvent_t4246496483 * ___evt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareREC::editLastRecording(com.mob.EditResultEvent)
extern "C"  void ShareREC_editLastRecording_m942934280 (Il2CppObject * __this /* static, unused */, EditResultEvent_t127194420 * ___evt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareREC::setSyncAudioComment(System.Boolean)
extern "C"  void ShareREC_setSyncAudioComment_m3360636254 (Il2CppObject * __this /* static, unused */, bool ___flag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.ShareREC::openSocial(System.String,System.Collections.Hashtable,com.mob.SocialPageType,com.mob.CloseEvent)
extern "C"  void ShareREC_openSocial_m2040620200 (Il2CppObject * __this /* static, unused */, String_t* ___title, Hashtable_t3875263730 * ___userData, int32_t ___pageType, CloseEvent_t4246496483 * ___evt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
