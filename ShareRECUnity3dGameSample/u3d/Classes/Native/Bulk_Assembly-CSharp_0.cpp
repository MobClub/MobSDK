﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Demo
struct Demo_t2126339;
// System.Exception
struct Exception_t1967233988;
// WebCamera
struct WebCamera_t387566457;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1217738301;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.String
struct String_t;
// WebCamera/<GetCamera>c__Iterator0
struct U3CGetCameraU3Ec__Iterator0_t2229711262;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharp_Demo2126339.h"
#include "AssemblyU2DCSharp_Demo2126339MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time1525492538MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application450040189MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3994030297MethodDeclarations.h"
#include "mscorlib_System_Single958209021.h"
#include "mscorlib_System_Int322847414787.h"
#include "UnityEngine_UnityEngine_ScreenOrientation1394230769.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_ShareREC3659413906MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Boolean211005341.h"
#include "UnityEngine_UnityEngine_GUI1522956648MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkin2614611333MethodDeclarations.h"
#include "mscorlib_System_Convert1097883944MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle1006925219MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect1525428817MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1588791936MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_FinishedReco4162477592MethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable3875263730MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_FinishedReco4162477592.h"
#include "mscorlib_System_Collections_Hashtable3875263730.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1574985880.h"
#include "UnityEngine_UnityEngine_GUISkin2614611333.h"
#include "UnityEngine_UnityEngine_GUIStyle1006925219.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_CloseEvent4246496483.h"
#include "AssemblyU2DCSharpU2Dfirstpass_com_mob_SocialPageTyp127466071.h"
#include "AssemblyU2DCSharp_WebCamera387566457.h"
#include "AssemblyU2DCSharp_WebCamera387566457MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer1092684080MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "UnityEngine_UnityEngine_MeshRenderer1217738301.h"
#include "UnityEngine_UnityEngine_Material1886596500.h"
#include "UnityEngine_UnityEngine_Coroutine2246592261.h"
#include "AssemblyU2DCSharp_WebCamera_U3CGetCameraU3Ec__Iter2229711262MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebCamera_U3CGetCameraU3Ec__Iter2229711262.h"
#include "UnityEngine_UnityEngine_WebCamTexture3635181805MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WebCamTexture3635181805.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material1886596500MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture1769722184.h"
#include "UnityEngine_UnityEngine_Texture2D2509538522MethodDeclarations.h"
#include "mscorlib_System_IO_FileStream1527309539MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D2509538522.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte2778693821.h"
#include "mscorlib_System_IO_FileStream1527309539.h"
#include "UnityEngine_UnityEngine_Texture1769722184MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "mscorlib_System_IO_FileMode1356058118.h"
#include "mscorlib_System_IO_Stream219029575MethodDeclarations.h"
#include "mscorlib_System_IO_Stream219029575.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WebCamDevice1687076478MethodDeclarations.h"
#include "mscorlib_System_UInt32985925326.h"
#include "UnityEngine_UnityEngine_AsyncOperation3374395064.h"
#include "UnityEngine_UnityEngine_UserAuthorization1561365147.h"
#include "UnityEngine_UnityEngine_WebCamDevice1687076478.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2447772384(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.MeshRenderer>()
#define GameObject_GetComponent_TisMeshRenderer_t1217738301_m2686897910(__this, method) ((  MeshRenderer_t1217738301 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Demo::.ctor()
extern "C"  void Demo__ctor_m2996546552 (Demo_t2126339 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Demo::Awake()
extern "C"  void Demo_Awake_m3234151771 (Demo_t2126339 * __this, const MethodInfo* method)
{
	{
		Time_set_fixedDeltaTime_m2294328761(NULL /*static, unused*/, (0.02f), /*hidden argument*/NULL);
		Application_set_targetFrameRate_m498658007(NULL /*static, unused*/, ((int32_t)30), /*hidden argument*/NULL);
		Screen_set_sleepTimeout_m4188281051(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
		Screen_set_orientation_m931760051(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Demo::Start()
extern Il2CppCodeGenString* _stringLiteral3404924862;
extern const uint32_t Demo_Start_m1943684344_MetadataUsageId;
extern "C"  void Demo_Start_m1943684344 (Demo_t2126339 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Demo_Start_m1943684344_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ShareREC_registerApp_m104679203(NULL /*static, unused*/, _stringLiteral3404924862, /*hidden argument*/NULL);
		ShareREC_setSyncAudioComment_m3360636254(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Demo::Update()
extern "C"  void Demo_Update_m130524693 (Demo_t2126339 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Demo::OnGUI()
extern TypeInfo* GUI_t1522956648_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1097883944_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern TypeInfo* FinishedRecordEvent_t4162477592_il2cpp_TypeInfo_var;
extern TypeInfo* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern const MethodInfo* Demo_recordFinishedHandler_m2900816397_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2587682;
extern Il2CppCodeGenString* _stringLiteral80204866;
extern Il2CppCodeGenString* _stringLiteral1324195426;
extern Il2CppCodeGenString* _stringLiteral2481783693;
extern Il2CppCodeGenString* _stringLiteral109264530;
extern Il2CppCodeGenString* _stringLiteral46730161;
extern Il2CppCodeGenString* _stringLiteral1127030674;
extern const uint32_t Demo_OnGUI_m2491945202_MetadataUsageId;
extern "C"  void Demo_OnGUI_m2491945202 (Demo_t2126339 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Demo_OnGUI_m2491945202_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	FinishedRecordEvent_t4162477592 * V_4 = NULL;
	Hashtable_t3875263730 * V_5 = NULL;
	Rect_t1525428817  G_B4_0;
	memset(&G_B4_0, 0, sizeof(G_B4_0));
	Rect_t1525428817  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	String_t* G_B5_0 = NULL;
	Rect_t1525428817  G_B5_1;
	memset(&G_B5_1, 0, sizeof(G_B5_1));
	{
		V_0 = (1.0f);
		int32_t L_0 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = (((float)((float)((int32_t)((int32_t)L_1/(int32_t)((int32_t)320))))));
	}

IL_001e:
	{
		float L_2 = V_0;
		V_1 = ((float)((float)(200.0f)*(float)L_2));
		float L_3 = V_0;
		V_2 = ((float)((float)(45.0f)*(float)L_3));
		float L_4 = V_0;
		V_3 = ((float)((float)(20.0f)*(float)L_4));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUISkin_t2614611333 * L_5 = GUI_get_skin_m4001454842(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIStyle_t1006925219 * L_6 = GUISkin_get_button_m3628017756(L_5, /*hidden argument*/NULL);
		float L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		int32_t L_8 = Convert_ToInt32_m3766704696(NULL /*static, unused*/, ((float)((float)(16.0f)*(float)L_7)), /*hidden argument*/NULL);
		NullCheck(L_6);
		GUIStyle_set_fontSize_m3621764235(L_6, L_8, /*hidden argument*/NULL);
		int32_t L_9 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_10 = V_1;
		float L_11 = V_3;
		float L_12 = V_1;
		float L_13 = V_2;
		Rect_t1525428817  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Rect__ctor_m3291325233(&L_14, ((float)((float)((float)((float)(((float)((float)L_9)))-(float)L_10))/(float)(2.0f))), L_11, L_12, L_13, /*hidden argument*/NULL);
		bool L_15 = __this->get__hasRecord_2();
		G_B3_0 = L_14;
		if (!L_15)
		{
			G_B4_0 = L_14;
			goto IL_007c;
		}
	}
	{
		G_B5_0 = _stringLiteral2587682;
		G_B5_1 = G_B3_0;
		goto IL_0081;
	}

IL_007c:
	{
		G_B5_0 = _stringLiteral80204866;
		G_B5_1 = G_B4_0;
	}

IL_0081:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_16 = GUI_Button_m885093907(NULL /*static, unused*/, G_B5_1, G_B5_0, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00ce;
		}
	}
	{
		bool L_17 = __this->get__hasRecord_2();
		__this->set__hasRecord_2((bool)((((int32_t)L_17) == ((int32_t)0))? 1 : 0));
		bool L_18 = __this->get__hasRecord_2();
		if (!L_18)
		{
			goto IL_00af;
		}
	}
	{
		ShareREC_startRecoring_m2705032210(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_00ce;
	}

IL_00af:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1324195426, /*hidden argument*/NULL);
		IntPtr_t L_19;
		L_19.set_m_value_0((void*)Demo_recordFinishedHandler_m2900816397_MethodInfo_var);
		FinishedRecordEvent_t4162477592 * L_20 = (FinishedRecordEvent_t4162477592 *)il2cpp_codegen_object_new(FinishedRecordEvent_t4162477592_il2cpp_TypeInfo_var);
		FinishedRecordEvent__ctor_m3923931911(L_20, __this, L_19, /*hidden argument*/NULL);
		V_4 = L_20;
		FinishedRecordEvent_t4162477592 * L_21 = V_4;
		ShareREC_stopRecording_m2331859558(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
	}

IL_00ce:
	{
		float L_22 = V_3;
		float L_23 = V_2;
		float L_24 = V_0;
		V_3 = ((float)((float)L_22+(float)((float)((float)L_23+(float)((float)((float)(20.0f)*(float)L_24))))));
		int32_t L_25 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_26 = V_1;
		float L_27 = V_3;
		float L_28 = V_1;
		float L_29 = V_2;
		Rect_t1525428817  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Rect__ctor_m3291325233(&L_30, ((float)((float)((float)((float)(((float)((float)L_25)))-(float)L_26))/(float)(2.0f))), L_27, L_28, L_29, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_31 = GUI_Button_m885093907(NULL /*static, unused*/, L_30, _stringLiteral2481783693, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0124;
		}
	}
	{
		Hashtable_t3875263730 * L_32 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_32, /*hidden argument*/NULL);
		V_5 = L_32;
		Hashtable_t3875263730 * L_33 = V_5;
		NullCheck(L_33);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(24 /* System.Void System.Collections.Hashtable::set_Item(System.Object,System.Object) */, L_33, _stringLiteral109264530, _stringLiteral46730161);
		Hashtable_t3875263730 * L_34 = V_5;
		ShareREC_editLastingRecording_m3284923037(NULL /*static, unused*/, _stringLiteral1127030674, L_34, (CloseEvent_t4246496483 *)NULL, /*hidden argument*/NULL);
	}

IL_0124:
	{
		return;
	}
}
// System.Void Demo::recordFinishedHandler(System.Exception)
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern TypeInfo* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2550534368;
extern Il2CppCodeGenString* _stringLiteral1190699939;
extern const uint32_t Demo_recordFinishedHandler_m2900816397_MetadataUsageId;
extern "C"  void Demo_recordFinishedHandler_m2900816397 (Demo_t2126339 * __this, Exception_t1967233988 * ___ex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Demo_recordFinishedHandler_m2900816397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Hashtable_t3875263730 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2550534368, /*hidden argument*/NULL);
		Hashtable_t3875263730 * L_0 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Hashtable_t3875263730 * L_1 = V_0;
		ShareREC_openSocial_m2040620200(NULL /*static, unused*/, _stringLiteral1190699939, L_1, 0, (CloseEvent_t4246496483 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebCamera::.ctor()
extern "C"  void WebCamera__ctor_m93069842 (WebCamera_t387566457 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebCamera::Start()
extern const MethodInfo* GameObject_GetComponent_TisMeshRenderer_t1217738301_m2686897910_MethodInfo_var;
extern const uint32_t WebCamera_Start_m3335174930_MetadataUsageId;
extern "C"  void WebCamera_Start_m3335174930 (WebCamera_t387566457 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebCamera_Start_m3335174930_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = __this->get_plane_2();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		GameObject_t4012695102 * L_2 = __this->get_plane_2();
		NullCheck(L_2);
		MeshRenderer_t1217738301 * L_3 = GameObject_GetComponent_TisMeshRenderer_t1217738301_m2686897910(L_2, /*hidden argument*/GameObject_GetComponent_TisMeshRenderer_t1217738301_m2686897910_MethodInfo_var);
		NullCheck(L_3);
		Material_t1886596500 * L_4 = Renderer_get_material_m2720864603(L_3, /*hidden argument*/NULL);
		__this->set_mMaterial_3(L_4);
		Material_t1886596500 * L_5 = __this->get_mMaterial_3();
		bool L_6 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_5, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003a;
		}
	}
	{
		return;
	}

IL_003a:
	{
		GameObject_t4012695102 * L_7 = __this->get_plane_2();
		NullCheck(L_7);
		GameObject_SetActive_m3538205401(L_7, (bool)0, /*hidden argument*/NULL);
		Il2CppObject * L_8 = WebCamera_GetCamera_m306297683(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebCamera::OnDestroy()
extern "C"  void WebCamera_OnDestroy_m3480919563 (WebCamera_t387566457 * __this, const MethodInfo* method)
{
	{
		WebCamera_StopCamera_m3000787001(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator WebCamera::GetCamera()
extern TypeInfo* U3CGetCameraU3Ec__Iterator0_t2229711262_il2cpp_TypeInfo_var;
extern const uint32_t WebCamera_GetCamera_m306297683_MetadataUsageId;
extern "C"  Il2CppObject * WebCamera_GetCamera_m306297683 (WebCamera_t387566457 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebCamera_GetCamera_m306297683_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetCameraU3Ec__Iterator0_t2229711262 * V_0 = NULL;
	{
		U3CGetCameraU3Ec__Iterator0_t2229711262 * L_0 = (U3CGetCameraU3Ec__Iterator0_t2229711262 *)il2cpp_codegen_object_new(U3CGetCameraU3Ec__Iterator0_t2229711262_il2cpp_TypeInfo_var);
		U3CGetCameraU3Ec__Iterator0__ctor_m3968592227(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetCameraU3Ec__Iterator0_t2229711262 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_5(__this);
		U3CGetCameraU3Ec__Iterator0_t2229711262 * L_2 = V_0;
		return L_2;
	}
}
// System.Void WebCamera::StartCamera()
extern "C"  void WebCamera_StartCamera_m866393175 (WebCamera_t387566457 * __this, const MethodInfo* method)
{
	{
		Material_t1886596500 * L_0 = __this->get_mMaterial_3();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		WebCamTexture_t3635181805 * L_2 = __this->get_mActiveCam_7();
		bool L_3 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_2, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0022:
	{
		return;
	}

IL_0023:
	{
		WebCamTexture_t3635181805 * L_4 = __this->get_mActiveCam_7();
		NullCheck(L_4);
		WebCamTexture_Play_m2252445503(L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebCamera::StopCamera()
extern "C"  void WebCamera_StopCamera_m3000787001 (WebCamera_t387566457 * __this, const MethodInfo* method)
{
	{
		Material_t1886596500 * L_0 = __this->get_mMaterial_3();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		WebCamTexture_t3635181805 * L_2 = __this->get_mActiveCam_7();
		bool L_3 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_2, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0022:
	{
		return;
	}

IL_0023:
	{
		WebCamTexture_t3635181805 * L_4 = __this->get_mActiveCam_7();
		NullCheck(L_4);
		WebCamTexture_Stop_m2346129549(L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WebCamera::HasFrontCamera()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t WebCamera_HasFrontCamera_m689030962_MetadataUsageId;
extern "C"  bool WebCamera_HasFrontCamera_m689030962 (WebCamera_t387566457 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebCamera_HasFrontCamera_m689030962_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isEditor_m1279348309(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return (bool)0;
	}

IL_000c:
	{
		WebCamTexture_t3635181805 * L_1 = __this->get_mFrontWebcamTexture_4();
		NullCheck(L_1);
		String_t* L_2 = WebCamTexture_get_deviceName_m1248945202(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_4 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void WebCamera::ChangeCamera()
extern Il2CppCodeGenString* _stringLiteral558922319;
extern const uint32_t WebCamera_ChangeCamera_m1349637543_MetadataUsageId;
extern "C"  void WebCamera_ChangeCamera_m1349637543 (WebCamera_t387566457 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebCamera_ChangeCamera_m1349637543_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = WebCamera_HasFrontCamera_m689030962(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		WebCamTexture_t3635181805 * L_1 = __this->get_mActiveCam_7();
		NullCheck(L_1);
		WebCamTexture_Stop_m2346129549(L_1, /*hidden argument*/NULL);
		WebCamTexture_t3635181805 * L_2 = __this->get_mActiveCam_7();
		WebCamTexture_t3635181805 * L_3 = __this->get_mFrontWebcamTexture_4();
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0054;
		}
	}
	{
		Material_t1886596500 * L_5 = __this->get_mMaterial_3();
		WebCamTexture_t3635181805 * L_6 = __this->get_mRearWebcamTexture_5();
		NullCheck(L_5);
		Material_SetTexture_m1833724755(L_5, _stringLiteral558922319, L_6, /*hidden argument*/NULL);
		WebCamTexture_t3635181805 * L_7 = __this->get_mRearWebcamTexture_5();
		__this->set_mActiveCam_7(L_7);
		goto IL_0076;
	}

IL_0054:
	{
		Material_t1886596500 * L_8 = __this->get_mMaterial_3();
		WebCamTexture_t3635181805 * L_9 = __this->get_mFrontWebcamTexture_4();
		NullCheck(L_8);
		Material_SetTexture_m1833724755(L_8, _stringLiteral558922319, L_9, /*hidden argument*/NULL);
		WebCamTexture_t3635181805 * L_10 = __this->get_mFrontWebcamTexture_4();
		__this->set_mActiveCam_7(L_10);
	}

IL_0076:
	{
		WebCamTexture_t3635181805 * L_11 = __this->get_mActiveCam_7();
		NullCheck(L_11);
		WebCamTexture_Play_m2252445503(L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebCamera::SavePhoto(System.String)
extern TypeInfo* Texture2D_t2509538522_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FileStream_t1527309539_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1481531;
extern Il2CppCodeGenString* _stringLiteral2661647777;
extern const uint32_t WebCamera_SavePhoto_m2894819837_MetadataUsageId;
extern "C"  void WebCamera_SavePhoto_m2894819837 (WebCamera_t387566457 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebCamera_SavePhoto_m2894819837_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Texture_t1769722184 * V_0 = NULL;
	Texture2D_t2509538522 * V_1 = NULL;
	ByteU5BU5D_t58506160* V_2 = NULL;
	String_t* V_3 = NULL;
	FileStream_t1527309539 * V_4 = NULL;
	{
		Material_t1886596500 * L_0 = __this->get_mMaterial_3();
		NullCheck(L_0);
		Texture_t1769722184 * L_1 = Material_get_mainTexture_m1012267054(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		WebCamTexture_t3635181805 * L_2 = __this->get_mActiveCam_7();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_2);
		WebCamTexture_t3635181805 * L_4 = __this->get_mActiveCam_7();
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_4);
		Texture2D_t2509538522 * L_6 = (Texture2D_t2509538522 *)il2cpp_codegen_object_new(Texture2D_t2509538522_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1883511258(L_6, L_3, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		Texture2D_t2509538522 * L_7 = V_1;
		WebCamTexture_t3635181805 * L_8 = __this->get_mActiveCam_7();
		NullCheck(L_8);
		ColorU5BU5D_t3477081137* L_9 = WebCamTexture_GetPixels_m2104673983(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Texture2D_SetPixels_m1289331147(L_7, L_9, /*hidden argument*/NULL);
		Texture2D_t2509538522 * L_10 = V_1;
		NullCheck(L_10);
		ByteU5BU5D_t58506160* L_11 = Texture2D_EncodeToPNG_m2464495756(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		String_t* L_12 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m138640077(NULL /*static, unused*/, L_12, _stringLiteral1481531, /*hidden argument*/NULL);
		V_3 = L_13;
		String_t* L_14 = V_3;
		String_t* L_15 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2661647777, L_14, /*hidden argument*/NULL);
		FileStream_t1527309539 * L_16 = (FileStream_t1527309539 *)il2cpp_codegen_object_new(FileStream_t1527309539_il2cpp_TypeInfo_var);
		FileStream__ctor_m3359540905(L_16, L_15, 2, /*hidden argument*/NULL);
		V_4 = L_16;
		FileStream_t1527309539 * L_17 = V_4;
		ByteU5BU5D_t58506160* L_18 = V_2;
		ByteU5BU5D_t58506160* L_19 = V_2;
		NullCheck(L_19);
		NullCheck(L_17);
		VirtActionInvoker3< ByteU5BU5D_t58506160*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.FileStream::Write(System.Byte[],System.Int32,System.Int32) */, L_17, L_18, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))));
		FileStream_t1527309539 * L_20 = V_4;
		NullCheck(L_20);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_20);
		return;
	}
}
// System.Void WebCamera/<GetCamera>c__Iterator0::.ctor()
extern "C"  void U3CGetCameraU3Ec__Iterator0__ctor_m3968592227 (U3CGetCameraU3Ec__Iterator0_t2229711262 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object WebCamera/<GetCamera>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetCameraU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3016651535 (U3CGetCameraU3Ec__Iterator0_t2229711262 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object WebCamera/<GetCamera>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetCameraU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2483975331 (U3CGetCameraU3Ec__Iterator0_t2229711262 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Boolean WebCamera/<GetCamera>c__Iterator0::MoveNext()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern TypeInfo* WebCamTexture_t3635181805_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral558922319;
extern const uint32_t U3CGetCameraU3Ec__Iterator0_MoveNext_m138777265_MetadataUsageId;
extern "C"  bool U3CGetCameraU3Ec__Iterator0_MoveNext_m138777265 (U3CGetCameraU3Ec__Iterator0_t2229711262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetCameraU3Ec__Iterator0_MoveNext_m138777265_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0039;
		}
	}
	{
		goto IL_0235;
	}

IL_0021:
	{
		AsyncOperation_t3374395064 * L_2 = Application_RequestUserAuthorization_m2737835758(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		__this->set_U24current_4(L_2);
		__this->set_U24PC_3(1);
		goto IL_0237;
	}

IL_0039:
	{
		bool L_3 = Application_HasUserAuthorization_m2306284210(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_022e;
		}
	}
	{
		WebCamera_t387566457 * L_4 = __this->get_U3CU3Ef__this_5();
		WebCamDeviceU5BU5D_t699707851* L_5 = WebCamTexture_get_devices_m3738445840(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_mDevices_6(L_5);
		WebCamera_t387566457 * L_6 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_6);
		WebCamDeviceU5BU5D_t699707851* L_7 = L_6->get_mDevices_6();
		if (!L_7)
		{
			goto IL_022e;
		}
	}
	{
		WebCamera_t387566457 * L_8 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_8);
		WebCamDeviceU5BU5D_t699707851* L_9 = L_8->get_mDevices_6();
		NullCheck(L_9);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_022e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_U3CfrontCamNameU3E__0_0(L_10);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_U3CrearCamNameU3E__1_1(L_11);
		__this->set_U3CiU3E__2_2(0);
		goto IL_0128;
	}

IL_0099:
	{
		WebCamera_t387566457 * L_12 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_12);
		WebCamDeviceU5BU5D_t699707851* L_13 = L_12->get_mDevices_6();
		int32_t L_14 = __this->get_U3CiU3E__2_2();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		bool L_15 = WebCamDevice_get_isFrontFacing_m3402023620(((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_14))), /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00df;
		}
	}
	{
		WebCamera_t387566457 * L_16 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_16);
		WebCamDeviceU5BU5D_t699707851* L_17 = L_16->get_mDevices_6();
		int32_t L_18 = __this->get_U3CiU3E__2_2();
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		String_t* L_19 = WebCamDevice_get_name_m2875559007(((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), /*hidden argument*/NULL);
		__this->set_U3CfrontCamNameU3E__0_0(L_19);
		goto IL_0100;
	}

IL_00df:
	{
		WebCamera_t387566457 * L_20 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_20);
		WebCamDeviceU5BU5D_t699707851* L_21 = L_20->get_mDevices_6();
		int32_t L_22 = __this->get_U3CiU3E__2_2();
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		String_t* L_23 = WebCamDevice_get_name_m2875559007(((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_22))), /*hidden argument*/NULL);
		__this->set_U3CrearCamNameU3E__1_1(L_23);
	}

IL_0100:
	{
		WebCamDeviceU5BU5D_t699707851* L_24 = WebCamTexture_get_devices_m3738445840(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_25 = __this->get_U3CiU3E__2_2();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		String_t* L_26 = WebCamDevice_get_name_m2875559007(((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_25))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		int32_t L_27 = __this->get_U3CiU3E__2_2();
		__this->set_U3CiU3E__2_2(((int32_t)((int32_t)L_27+(int32_t)1)));
	}

IL_0128:
	{
		int32_t L_28 = __this->get_U3CiU3E__2_2();
		WebCamera_t387566457 * L_29 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_29);
		WebCamDeviceU5BU5D_t699707851* L_30 = L_29->get_mDevices_6();
		NullCheck(L_30);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length)))))))
		{
			goto IL_0099;
		}
	}
	{
		WebCamera_t387566457 * L_31 = __this->get_U3CU3Ef__this_5();
		String_t* L_32 = __this->get_U3CfrontCamNameU3E__0_0();
		WebCamTexture_t3635181805 * L_33 = (WebCamTexture_t3635181805 *)il2cpp_codegen_object_new(WebCamTexture_t3635181805_il2cpp_TypeInfo_var);
		WebCamTexture__ctor_m3625407113(L_33, L_32, /*hidden argument*/NULL);
		NullCheck(L_31);
		L_31->set_mFrontWebcamTexture_4(L_33);
		WebCamera_t387566457 * L_34 = __this->get_U3CU3Ef__this_5();
		String_t* L_35 = __this->get_U3CrearCamNameU3E__1_1();
		WebCamTexture_t3635181805 * L_36 = (WebCamTexture_t3635181805 *)il2cpp_codegen_object_new(WebCamTexture_t3635181805_il2cpp_TypeInfo_var);
		WebCamTexture__ctor_m3625407113(L_36, L_35, /*hidden argument*/NULL);
		NullCheck(L_34);
		L_34->set_mRearWebcamTexture_5(L_36);
		WebCamera_t387566457 * L_37 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_37);
		WebCamTexture_t3635181805 * L_38 = L_37->get_mFrontWebcamTexture_4();
		NullCheck(L_38);
		WebCamTexture_Stop_m2346129549(L_38, /*hidden argument*/NULL);
		WebCamera_t387566457 * L_39 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_39);
		WebCamTexture_t3635181805 * L_40 = L_39->get_mRearWebcamTexture_5();
		NullCheck(L_40);
		WebCamTexture_Stop_m2346129549(L_40, /*hidden argument*/NULL);
		String_t* L_41 = __this->get_U3CfrontCamNameU3E__0_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_42 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		if (L_42)
		{
			goto IL_01d7;
		}
	}
	{
		WebCamera_t387566457 * L_43 = __this->get_U3CU3Ef__this_5();
		WebCamera_t387566457 * L_44 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_44);
		WebCamTexture_t3635181805 * L_45 = L_44->get_mFrontWebcamTexture_4();
		NullCheck(L_43);
		L_43->set_mActiveCam_7(L_45);
		WebCamera_t387566457 * L_46 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_46);
		Material_t1886596500 * L_47 = L_46->get_mMaterial_3();
		WebCamera_t387566457 * L_48 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_48);
		WebCamTexture_t3635181805 * L_49 = L_48->get_mFrontWebcamTexture_4();
		NullCheck(L_47);
		Material_SetTexture_m1833724755(L_47, _stringLiteral558922319, L_49, /*hidden argument*/NULL);
		goto IL_020d;
	}

IL_01d7:
	{
		WebCamera_t387566457 * L_50 = __this->get_U3CU3Ef__this_5();
		WebCamera_t387566457 * L_51 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_51);
		WebCamTexture_t3635181805 * L_52 = L_51->get_mRearWebcamTexture_5();
		NullCheck(L_50);
		L_50->set_mActiveCam_7(L_52);
		WebCamera_t387566457 * L_53 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_53);
		Material_t1886596500 * L_54 = L_53->get_mMaterial_3();
		WebCamera_t387566457 * L_55 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_55);
		WebCamTexture_t3635181805 * L_56 = L_55->get_mRearWebcamTexture_5();
		NullCheck(L_54);
		Material_SetTexture_m1833724755(L_54, _stringLiteral558922319, L_56, /*hidden argument*/NULL);
	}

IL_020d:
	{
		WebCamera_t387566457 * L_57 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_57);
		GameObject_t4012695102 * L_58 = L_57->get_plane_2();
		NullCheck(L_58);
		GameObject_SetActive_m3538205401(L_58, (bool)1, /*hidden argument*/NULL);
		WebCamera_t387566457 * L_59 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_59);
		WebCamTexture_t3635181805 * L_60 = L_59->get_mActiveCam_7();
		NullCheck(L_60);
		WebCamTexture_Play_m2252445503(L_60, /*hidden argument*/NULL);
	}

IL_022e:
	{
		__this->set_U24PC_3((-1));
	}

IL_0235:
	{
		return (bool)0;
	}

IL_0237:
	{
		return (bool)1;
	}
	// Dead block : IL_0239: ldloc.1
}
// System.Void WebCamera/<GetCamera>c__Iterator0::Dispose()
extern "C"  void U3CGetCameraU3Ec__Iterator0_Dispose_m4079421664 (U3CGetCameraU3Ec__Iterator0_t2229711262 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void WebCamera/<GetCamera>c__Iterator0::Reset()
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetCameraU3Ec__Iterator0_Reset_m1615025168_MetadataUsageId;
extern "C"  void U3CGetCameraU3Ec__Iterator0_Reset_m1615025168 (U3CGetCameraU3Ec__Iterator0_t2229711262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetCameraU3Ec__Iterator0_Reset_m1615025168_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
