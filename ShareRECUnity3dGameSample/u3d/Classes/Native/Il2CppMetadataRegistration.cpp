﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"


extern Il2CppGenericClass* const s_Il2CppGenericTypes[];
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[];
extern const Il2CppGenericMethodFunctionsDefinitions s_Il2CppGenericMethodFunctions[];
extern const Il2CppType* const  g_Il2CppTypeTable[];
extern const Il2CppMethodSpec g_Il2CppMethodSpecTable[];
extern const int32_t g_FieldOffsetTable[];
extern const Il2CppTypeDefinitionSizes g_Il2CppTypeDefinitionSizesTable[];
extern void** const g_MetadataUsages[];
extern const Il2CppMetadataRegistration g_MetadataRegistration = 
{
	1781,
	s_Il2CppGenericTypes,
	504,
	g_Il2CppGenericInstTable,
	3981,
	s_Il2CppGenericMethodFunctions,
	7003,
	g_Il2CppTypeTable,
	4253,
	g_Il2CppMethodSpecTable,
	6470,
	g_FieldOffsetTable,
	1642,
	g_Il2CppTypeDefinitionSizesTable,
	8580,
	g_MetadataUsages,
};
