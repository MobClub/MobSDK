﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.Material
struct Material_t1886596500;
// UnityEngine.WebCamTexture
struct WebCamTexture_t3635181805;
// UnityEngine.WebCamDevice[]
struct WebCamDeviceU5BU5D_t699707851;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebCamera
struct  WebCamera_t387566457  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.GameObject WebCamera::plane
	GameObject_t4012695102 * ___plane_2;
	// UnityEngine.Material WebCamera::mMaterial
	Material_t1886596500 * ___mMaterial_3;
	// UnityEngine.WebCamTexture WebCamera::mFrontWebcamTexture
	WebCamTexture_t3635181805 * ___mFrontWebcamTexture_4;
	// UnityEngine.WebCamTexture WebCamera::mRearWebcamTexture
	WebCamTexture_t3635181805 * ___mRearWebcamTexture_5;
	// UnityEngine.WebCamDevice[] WebCamera::mDevices
	WebCamDeviceU5BU5D_t699707851* ___mDevices_6;
	// UnityEngine.WebCamTexture WebCamera::mActiveCam
	WebCamTexture_t3635181805 * ___mActiveCam_7;

public:
	inline static int32_t get_offset_of_plane_2() { return static_cast<int32_t>(offsetof(WebCamera_t387566457, ___plane_2)); }
	inline GameObject_t4012695102 * get_plane_2() const { return ___plane_2; }
	inline GameObject_t4012695102 ** get_address_of_plane_2() { return &___plane_2; }
	inline void set_plane_2(GameObject_t4012695102 * value)
	{
		___plane_2 = value;
		Il2CppCodeGenWriteBarrier(&___plane_2, value);
	}

	inline static int32_t get_offset_of_mMaterial_3() { return static_cast<int32_t>(offsetof(WebCamera_t387566457, ___mMaterial_3)); }
	inline Material_t1886596500 * get_mMaterial_3() const { return ___mMaterial_3; }
	inline Material_t1886596500 ** get_address_of_mMaterial_3() { return &___mMaterial_3; }
	inline void set_mMaterial_3(Material_t1886596500 * value)
	{
		___mMaterial_3 = value;
		Il2CppCodeGenWriteBarrier(&___mMaterial_3, value);
	}

	inline static int32_t get_offset_of_mFrontWebcamTexture_4() { return static_cast<int32_t>(offsetof(WebCamera_t387566457, ___mFrontWebcamTexture_4)); }
	inline WebCamTexture_t3635181805 * get_mFrontWebcamTexture_4() const { return ___mFrontWebcamTexture_4; }
	inline WebCamTexture_t3635181805 ** get_address_of_mFrontWebcamTexture_4() { return &___mFrontWebcamTexture_4; }
	inline void set_mFrontWebcamTexture_4(WebCamTexture_t3635181805 * value)
	{
		___mFrontWebcamTexture_4 = value;
		Il2CppCodeGenWriteBarrier(&___mFrontWebcamTexture_4, value);
	}

	inline static int32_t get_offset_of_mRearWebcamTexture_5() { return static_cast<int32_t>(offsetof(WebCamera_t387566457, ___mRearWebcamTexture_5)); }
	inline WebCamTexture_t3635181805 * get_mRearWebcamTexture_5() const { return ___mRearWebcamTexture_5; }
	inline WebCamTexture_t3635181805 ** get_address_of_mRearWebcamTexture_5() { return &___mRearWebcamTexture_5; }
	inline void set_mRearWebcamTexture_5(WebCamTexture_t3635181805 * value)
	{
		___mRearWebcamTexture_5 = value;
		Il2CppCodeGenWriteBarrier(&___mRearWebcamTexture_5, value);
	}

	inline static int32_t get_offset_of_mDevices_6() { return static_cast<int32_t>(offsetof(WebCamera_t387566457, ___mDevices_6)); }
	inline WebCamDeviceU5BU5D_t699707851* get_mDevices_6() const { return ___mDevices_6; }
	inline WebCamDeviceU5BU5D_t699707851** get_address_of_mDevices_6() { return &___mDevices_6; }
	inline void set_mDevices_6(WebCamDeviceU5BU5D_t699707851* value)
	{
		___mDevices_6 = value;
		Il2CppCodeGenWriteBarrier(&___mDevices_6, value);
	}

	inline static int32_t get_offset_of_mActiveCam_7() { return static_cast<int32_t>(offsetof(WebCamera_t387566457, ___mActiveCam_7)); }
	inline WebCamTexture_t3635181805 * get_mActiveCam_7() const { return ___mActiveCam_7; }
	inline WebCamTexture_t3635181805 ** get_address_of_mActiveCam_7() { return &___mActiveCam_7; }
	inline void set_mActiveCam_7(WebCamTexture_t3635181805 * value)
	{
		___mActiveCam_7 = value;
		Il2CppCodeGenWriteBarrier(&___mActiveCam_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
