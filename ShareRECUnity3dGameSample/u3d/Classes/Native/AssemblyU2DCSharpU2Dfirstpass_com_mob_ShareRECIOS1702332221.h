﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// com.mob.FinishedRecordEvent
struct FinishedRecordEvent_t4162477592;
// com.mob.CloseEvent
struct CloseEvent_t4246496483;
// com.mob.EditResultEvent
struct EditResultEvent_t127194420;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// com.mob.ShareRECIOS
struct  ShareRECIOS_t1702332221  : public Il2CppObject
{
public:

public:
};

struct ShareRECIOS_t1702332221_StaticFields
{
public:
	// System.String com.mob.ShareRECIOS::_callbackObjectName
	String_t* ____callbackObjectName_0;
	// com.mob.FinishedRecordEvent com.mob.ShareRECIOS::_finishedRecordHandler
	FinishedRecordEvent_t4162477592 * ____finishedRecordHandler_1;
	// com.mob.CloseEvent com.mob.ShareRECIOS::_closeHandler
	CloseEvent_t4246496483 * ____closeHandler_2;
	// com.mob.EditResultEvent com.mob.ShareRECIOS::_editResultHandler
	EditResultEvent_t127194420 * ____editResultHandler_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> com.mob.ShareRECIOS::<>f__switch$map0
	Dictionary_2_t190145395 * ___U3CU3Ef__switchU24map0_4;

public:
	inline static int32_t get_offset_of__callbackObjectName_0() { return static_cast<int32_t>(offsetof(ShareRECIOS_t1702332221_StaticFields, ____callbackObjectName_0)); }
	inline String_t* get__callbackObjectName_0() const { return ____callbackObjectName_0; }
	inline String_t** get_address_of__callbackObjectName_0() { return &____callbackObjectName_0; }
	inline void set__callbackObjectName_0(String_t* value)
	{
		____callbackObjectName_0 = value;
		Il2CppCodeGenWriteBarrier(&____callbackObjectName_0, value);
	}

	inline static int32_t get_offset_of__finishedRecordHandler_1() { return static_cast<int32_t>(offsetof(ShareRECIOS_t1702332221_StaticFields, ____finishedRecordHandler_1)); }
	inline FinishedRecordEvent_t4162477592 * get__finishedRecordHandler_1() const { return ____finishedRecordHandler_1; }
	inline FinishedRecordEvent_t4162477592 ** get_address_of__finishedRecordHandler_1() { return &____finishedRecordHandler_1; }
	inline void set__finishedRecordHandler_1(FinishedRecordEvent_t4162477592 * value)
	{
		____finishedRecordHandler_1 = value;
		Il2CppCodeGenWriteBarrier(&____finishedRecordHandler_1, value);
	}

	inline static int32_t get_offset_of__closeHandler_2() { return static_cast<int32_t>(offsetof(ShareRECIOS_t1702332221_StaticFields, ____closeHandler_2)); }
	inline CloseEvent_t4246496483 * get__closeHandler_2() const { return ____closeHandler_2; }
	inline CloseEvent_t4246496483 ** get_address_of__closeHandler_2() { return &____closeHandler_2; }
	inline void set__closeHandler_2(CloseEvent_t4246496483 * value)
	{
		____closeHandler_2 = value;
		Il2CppCodeGenWriteBarrier(&____closeHandler_2, value);
	}

	inline static int32_t get_offset_of__editResultHandler_3() { return static_cast<int32_t>(offsetof(ShareRECIOS_t1702332221_StaticFields, ____editResultHandler_3)); }
	inline EditResultEvent_t127194420 * get__editResultHandler_3() const { return ____editResultHandler_3; }
	inline EditResultEvent_t127194420 ** get_address_of__editResultHandler_3() { return &____editResultHandler_3; }
	inline void set__editResultHandler_3(EditResultEvent_t127194420 * value)
	{
		____editResultHandler_3 = value;
		Il2CppCodeGenWriteBarrier(&____editResultHandler_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_4() { return static_cast<int32_t>(offsetof(ShareRECIOS_t1702332221_StaticFields, ___U3CU3Ef__switchU24map0_4)); }
	inline Dictionary_2_t190145395 * get_U3CU3Ef__switchU24map0_4() const { return ___U3CU3Ef__switchU24map0_4; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CU3Ef__switchU24map0_4() { return &___U3CU3Ef__switchU24map0_4; }
	inline void set_U3CU3Ef__switchU24map0_4(Dictionary_2_t190145395 * value)
	{
		___U3CU3Ef__switchU24map0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map0_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
