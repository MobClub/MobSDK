﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// com.mob.FinishedRecordEvent
struct FinishedRecordEvent_t4162477592;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void com.mob.FinishedRecordEvent::.ctor(System.Object,System.IntPtr)
extern "C"  void FinishedRecordEvent__ctor_m3923931911 (FinishedRecordEvent_t4162477592 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.FinishedRecordEvent::Invoke(System.Exception)
extern "C"  void FinishedRecordEvent_Invoke_m3382741201 (FinishedRecordEvent_t4162477592 * __this, Exception_t1967233988 * ___ex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_FinishedRecordEvent_t4162477592(Il2CppObject* delegate, Exception_t1967233988 * ___ex);
// System.IAsyncResult com.mob.FinishedRecordEvent::BeginInvoke(System.Exception,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FinishedRecordEvent_BeginInvoke_m1902627620 (FinishedRecordEvent_t4162477592 * __this, Exception_t1967233988 * ___ex, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.mob.FinishedRecordEvent::EndInvoke(System.IAsyncResult)
extern "C"  void FinishedRecordEvent_EndInvoke_m1392410775 (FinishedRecordEvent_t4162477592 * __this, Il2CppObject * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
